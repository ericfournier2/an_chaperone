export RAP_ID="eav-760-aa"

mkdir -p output/pipeline

$MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.py -j slurm -s '1-4' \
    -l debug \
    -r raw/readset.txt \
    -d raw/design_B.txt \
    -o output/pipeline \
    --config $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.cedar.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Homo_sapiens.GRCh38.ini \
        input/chipseq.numpy.bug.ini        