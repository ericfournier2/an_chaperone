
source("scripts/correlation_plots.R")


# Generate a design which groups samples into an input/control design
# for metagene generation.
build_ratio_design = function(use_replicates) {
    # If use_replicates is TRUE, we'll generate one ratio per replicate.
    # Otherwise we'll generate a ratio of means.
    if(use_replicates) {
        rep_list = unique(fileset_exch$Replicate)
    } else {
        rep_list="Replicate mean"
    }

    # Initialize the data-frames.
    design = data.frame(BAM=fileset_exch$BAM_name)
    design_metadata = data.frame(design=character(),
                                 Antibody=character(),
                                 Replicate=integer())
    
    # Loop over antibody, replicates. If we're generating a mean of 
    # replicates, then rep_list contains a single dummy value.
    for(antibody in unique(fileset_exch$Antibody)) {
        for(rep_id in rep_list) {
            # Generate a design name, initialize it to zero.
            design_name = paste0(antibody, "-", rep_id)
            design[[design_name]] = 0
            
            # Build indices vectors for choosing the right bam files.
            right_antibody = fileset_exch$Antibody == antibody
            ck2 = fileset_exch$Strain=="CK2"
            wt = fileset_exch$Strain=="WT"
            if(use_replicates) {
                right_replicate = fileset_exch$Replicate == rep_id
            } else {
                right_replicate = TRUE
            }

            # Set the design's values: 1 for input, 2 for control.
            design[right_antibody & right_replicate & ck2, design_name] = 1
            design[right_antibody & right_replicate & wt, design_name] = 2
            
            # Append to the design metadata.
            metadata_row = data.frame(design=design_name, Antibody=antibody, Replicate=rep_id)
            design_metadata = rbind(design_metadata, metadata_row)
        }
    } 
    
    # Turn Replicate into a factor and add StrainReplicate, which are both
    # used for plotting.
    design_metadata$Replicate = factor(design_metadata$Replicate)
    design_metadata$StrainReplicate = paste0("Ratio-", design_metadata$Replicate)

    # Return the design and its metadata.
    return(list(design=design, metadata=design_metadata))
}

# Determine the 3' on 5' quintile for each gene.
make_ratio_quintile <- function(ratio_metric) {
    metric_mean = rowMeans(cbind(ratio_metric[["Cl1"]], 
                                 ratio_metric[["Cl1"]]))
    tmp.df = data.frame(Index=1:length(metric_mean), Metric=metric_mean)
    tmp.df = tmp.df[is.finite(tmp.df$Metric),]
    
    q.split = quantile(tmp.df$Metric, seq(0,1,0.2))
    tmp.df$Quintile = cut(tmp.df$Metric, q.split, include.lowest=TRUE)
    levels(tmp.df$Quintile) =  paste0("Q", 5:1)
    
    res = rep(NA_character_, length(ratio_metric))
    res[tmp.df$Index] = as.character(tmp.df$Quintile)
    
    res = factor(res, levels=paste0("Q", 5:1))
}

coordinates$ThreeOnFiveQuintile = make_ratio_quintile(CK2_on_wt_three_on_five_ratio)
coordinates$AntisenseOnSenseQuintile = make_ratio_quintile(CK2_on_wt_antisense_on_sense_ratio)

# Add the 3'/5' info to the region metadata so we can use it for facetting.
mg_list[["Exchange"]]$replace_region_metadata(mcols(coordinates))

# Plot ratio metagenes both for individual replicates, and for mean
# of conditions.
ratio_analysis = list(Mean=FALSE, "Individual Replicates"=TRUE)
ratio_scale_values =  c("Ratio-1"="#c10b0b", 
                        "Ratio-Replicate mean"="#fe1919", 
                        "Ratio-2"="#1c80ff")
for(i in names(ratio_analysis)) {
     ratio_design_list = build_ratio_design(ratio_analysis[[i]])
     plot_all_diagnostics(mg_list[["Exchange"]], 
                          color_scale=ratio_scale_values,
                          label=paste("Ratio", i),
                          invert=FALSE,
                          skip_log=TRUE,
                          design=ratio_design_list$design,
                          design_metadata=ratio_design_list$metadata,
                          normalization="log2_ratio")
}
