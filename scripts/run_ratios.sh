#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --mem=128G
#SBATCH --cpus-per-task=8
#SBATCH --account=def-amnou

module load r
cd ~/projects/def-amnou/efournie/Chaperone
Rscript scripts/ratio_metagenes.R

