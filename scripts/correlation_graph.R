library(metagene2)
library(rtracklayer)
library(GenomicFeatures)
library(ggplot2)

# Define regions:
COORDINATES = "BODIES"
FILTERED_BAM = TRUE
EXTEND = 200

# Get all genes.
coordinates = import("input/Saccharomyces_cerevisiae.R64-1-1.Ensembl77.gtf")
coordinates = coordinates[coordinates$type=="gene"]

if(COORDINATES=="BODIES_INTERGENIC") {
    # Generate intergenic gap, and add dummy annotation so we can concatenate it to the genes.
    nostrands = coordinates
    strand(nostrands)="+"
    
    intergenic = gaps(nostrands)
    for(i in names(mcols(coordinates))) {
        mcols(intergenic)[[i]] = NA
    }
    
    strand(intergenic) = "*"
    
    # Concatenate genes and intergenic regions.
    full.genome = c(coordinates, intergenic)
} else if(COORDINATES=="BODIES") {
    full.genome = c(coordinates)
} else {
    full.genome = promoters(invertStrand(coordinates), upstream=0, downstream=200)
}

regions = full.genome

# Define bam files:

# Read the readset file to infer bam names.
readset = read.table("raw/readset.txt", sep="\t", header=TRUE)
if(FILTERED_BAM) {
    bam = file.path("output/pipeline", "alignment", readset$Sample, paste0(readset$Sample, ".sorted.dup.bam"))
} else {
    bam = file.path("output/pipeline", "alignment", readset$Sample, readset$Readset, paste0(readset$Readset, ".sorted.bam"))
}
readset$bam = bam


# Add a design column to use it as metadata.
readset$design= basename(gsub(".bam", "", readset$bam))

# Remove conditions which might not have been processed.
readset = readset[file.exists(bam),]

# Make sure all columns are factors so they can be used for facetting or grouping.
for(col in colnames(readset)) {
    readset[,col] = as.factor(readset[,col])
}

mg = metagene2$new(regions=regions, bam_files=as.character(readset$bam), 
                   extend_reads=EXTEND,
                   cores=7, assay="chipseq", sample_count=0)

bin_cov = mg$bin_coverages(bin_count=50)

df_list = list()
for(i in 1:(length(bin_cov))) {
    for(j in 1:length(bin_cov)) {
        df_list = c(df_list,
                    list(data.frame(X=names(bin_cov)[i],
                                    Y=names(bin_cov)[j],
                                    Cor=cor(c(bin_cov[[i]]), c(bin_cov[[j]])))))
    }
}

cor_df = do.call(rbind, df_list)
cor_df$XOrig = cor_df$X
cor_df$YOrig = cor_df$Y
cor_df$X = gsub(".sorted.dup", "", cor_df$X)
cor_df$Y = gsub(".sorted.dup", "", cor_df$Y)
cor_df$X = gsub("-Myc", "", cor_df$X)
cor_df$Y = gsub("-Myc", "", cor_df$Y)
cor_df$Antibody_X = gsub("(.*?)[_](.*)", "\\1", cor_df$X)
cor_df$Antibody_Y = gsub("(.*?)[_](.*)", "\\1", cor_df$Y)
cor_df$Sample_X = gsub("(.*?)[_](.*)", "\\2", cor_df$X)
cor_df$Sample_Y = gsub("(.*?)[_](.*)", "\\2", cor_df$Y)
cor_df$Sample_NoClone_X = gsub("^([a-zA-Z0-9]+)_(.*?)_*(Cl.)$", "\\2", cor_df$X)
cor_df$Sample_NoClone_Y = gsub("^([a-zA-Z0-9]+)_(.*?)_*(Cl.)$", "\\2", cor_df$Y)




pdf("Full correlation graph.pdf", width=14)
ggplot(cor_df, aes(x=X, y=Y, fill=Cor, label=sprintf("%.2f", Cor))) +
    geom_tile() +
    geom_text() +
    scale_fill_gradient2(limits=c(-1,1), midpoint=0) + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()

for(antibody in unique(cor_df$Antibody_X)) {
    pdf(paste0("Correlation graph ", antibody, ".pdf"))
    cor_subset = cor_df$Antibody_X==antibody & cor_df$Antibody_Y==antibody
    gg_obj = ggplot(cor_df[cor_subset,], aes(x=Sample_X, y=Sample_Y, fill=Cor, label=sprintf("%.2f", Cor))) +
        geom_tile() +
        geom_text() +
        scale_fill_gradient2(limits=c(-1,1), midpoint=0) + 
        theme(axis.text.x = element_text(angle = 90, hjust = 1))    
    print(gg_obj)
    dev.off()
}    

library(dplyr)
cor_subset = cor_df %>% 
                filter(Antibody_X %in% c("Spt6", "Iws1")) %>%
                filter(Antibody_Y %in% c("Spt6", "Iws1")) %>%
                filter(Sample_NoClone_X %in% c("ckII_Ctl", "WT_Ctl")) %>%
                filter(Sample_NoClone_Y %in% c("ckII_Ctl", "WT_Ctl"))


 
factor_levels = c("Iws1_ckII_Ctl_Cl2", "Iws1_ckII_Ctl_Cl3", 
                  "Spt6_ckII_Ctl_Cl2", "Spt6_ckII_Ctl_Cl3", 
                  "Iws1_WT_Ctl_Cl1", "Iws1_WT_Ctl_Cl3", 
                  "Spt6_WT_Ctl_Cl1", "Spt6_WT_Ctl_Cl3")
                
cor_subset$X = factor(cor_subset$X, levels=factor_levels)
cor_subset$Y = factor(cor_subset$Y, levels=factor_levels)
pdf(paste0("Correlation graph comparing Iws1 and Spt6 CkII vs WT.pdf"), width=10, height=10)
gg_obj = ggplot(cor_subset, aes(x=X, y=Y, fill=Cor, label=sprintf("%.2f", Cor))) +
    geom_tile() +
    geom_text() +
    scale_fill_gradient(limits=c(0.7,1), low="white", high=scales::muted("blue")) + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1))    
print(gg_obj)
dev.off()

cor_subset = cor_df %>% 
                filter(Antibody_X %in% c("Spt6", "Iws1")) %>%
                filter(Antibody_Y %in% c("Spt6", "Iws1")) %>%
                filter(Sample_NoClone_X %in% c("spt6-SA_39C", "WT_39C")) %>%
                filter(Sample_NoClone_Y %in% c("spt6-SA_39C", "WT_39C"))


 
factor_levels = c("Iws1_spt6-SA_39C_Cl1", "Iws1_spt6-SA_39C_Cl3",
                  "Spt6_spt6-SA_39C_Cl1", "Spt6_spt6-SA_39C_Cl3",
                  "Iws1_WT_39C_Cl1", "Iws1_WT_39C_Cl3",
                  "Spt6_WT_39C_Cl1", "Spt6_WT_39C_Cl3")
                
cor_subset$X = factor(cor_subset$X, levels=factor_levels)
cor_subset$Y = factor(cor_subset$Y, levels=factor_levels)
pdf(paste0("Correlation graph comparing Iws1 and Spt6 Spt6-SA vs WT.pdf"), width=10, height=10)
gg_obj = ggplot(cor_subset, aes(x=X, y=Y, fill=Cor, label=sprintf("%.2f", Cor))) +
    geom_tile() +
    geom_text() +
    scale_fill_gradient(limits=c(0.7,1), low="white", high=scales::muted("blue")) + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1))    
print(gg_obj)
dev.off()

