for bam in output/pipeline/alignment/*/*.bam
do
    # Figure out sample name
    bamdir=`dirname $bam`
    samplename=`basename $bamdir`
    outputdir=output/pipeline/alignment-noMito/$samplename
    mkdir -p $outputdir
    outputbam=$outputdir/$samplename.sorted.dup.bam

    samtools idxstats $bam | cut -f 1 | grep -v Mito | xargs samtools view -b $bam > $outputbam
done
    
    