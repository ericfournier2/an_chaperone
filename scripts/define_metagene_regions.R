library(metagene2)
#library(TxDb.Scerevisiae.UCSC.sacCer3.sgdGene)
library(rtracklayer)
library(BSgenome.Scerevisiae.UCSC.sacCer3)
library(ggplot2)

# Utility function to snap coordinates to chromosomes minimum/maximum
snapToChrStart <- function(regions) {
    start(regions) = pmax(start(regions), 1)
    return(regions)
}

snapToChrEnd <- function(regions, chr.lengths) {
    chr.end = chr.lengths[as.character(seqnames(regions))]
    end(regions) = pmin(end(regions), chr.end)
    return(regions)
}

snapToChrEdges <- function(regions, chr.lengths) {
    snapToChrEnd(snapToChrStart(regions), chr.lengths)
}

# Function to get region flanking TSS
tss.flank <- function(regions, before, after, chr.length, keep.smaller=FALSE) {
    # Only keep genes which have the minimum width
    kept.regions = regions
    kept.indices = TRUE
    if(!keep.smaller) {
        kept.indices = width(regions)>after
        kept.regions = regions[kept.indices] 
    }

    tss.regions.pre = flank(GRanges(kept.regions), width=before, start=TRUE, both=FALSE)
    tss.regions.post = flank(tss.regions.pre, width=after, start=FALSE, both=FALSE)
    
    tss.regions.start = ifelse(strand(tss.regions.pre)=="+", start(tss.regions.pre), start(tss.regions.post))
    tss.regions.end = ifelse(strand(tss.regions.pre)=="+", end(tss.regions.post), end(tss.regions.pre))
    tss.regions = GRanges(data.frame(seqnames=seqnames(tss.regions.pre),
                                     start=tss.regions.start,
                                     end=tss.regions.end,
                                     strand=strand(tss.regions.pre)))    
    tss.regions = snapToChrEdges(tss.regions, chr.length)
    mcols(tss.regions) = mcols(kept.regions)
    
    return(tss.regions)                                     
}

load_metagene_regions <- function(levelStyle="ensembl", expr_level_source="Holstege") {
    # Get coordinates for all genes.
    coordinates = rtracklayer::import("input/Saccharomyces_cerevisiae.R64-1-1.Ensembl77.gtf")
    coordinates = coordinates[coordinates$type=="gene"]
    coordinates$ChrType = ifelse(seqnames(coordinates)=="Mito" | seqnames(coordinates)=="chrM", "Mito", "Genome")
    
    # Read expression levels
    if(expr_level_source=="Holstege") {
        expr.levels = read.table("input/Holstege.txt", header=TRUE, sep="\t")
    } else if(expr_level_source=="Holstege_TranscriptionalFreq") {
        expr.levels = read.table("input/Holstege.txt", header=TRUE, sep="\t")
        expr.levels$ExpressionLevel = expr.levels$TranscriptionalFreq
    } else {
        # Read transcripts and convert to TPM.
        expr_data = read.table("output/pipeline-RNA-CKII-Spt6/DGE/rawCountMatrix.csv", sep="\t", header=TRUE, quote="", check.names=FALSE)
        for(i in 3:8) {
            expr_data[,i] = expr_data[,i] / sum(expr_data[,i]) * 1000000
        }
        expr.levels = data.frame(ORF=expr_data$Gene)
        if(expr_level_source=="CK2Mean") {
            expr.levels$ExpressionLevel=apply(expr_data[,c("CK2", "CK2-2")], 1, mean)
        } else if(expr_level_source=="WTMean") {
            expr.levels$ExpressionLevel=apply(expr_data[,c("WT", "WT-2")], 1, mean)
        } else if(expr_level_source=="allMean") {
            expr.levels$ExpressionLevel=apply(expr_data[,3:8], 1, mean)
        } else {
            stop("Invalid expr_level_source.")
        }
    }

    expr.levels = expr.levels[!is.na(expr.levels$ExpressionLevel),]
    expr.levels = expr.levels[order(expr.levels$ExpressionLevel),]
    
    # Determine splitting points (Q1, Q2, ... Q5)
    q.split = quantile(expr.levels$ExpressionLevel, seq(0,1,0.2))
    
    # Associate expression values with their quantile.
    expr.quintile = cut(expr.levels$ExpressionLevel, q.split, include.lowest=TRUE)
    expr.levels$Q = expr.quintile
    levels(expr.levels$Q) =  paste0("Q", 5:1)
    
    coordinates$Quintile = expr.levels$Q[match(coordinates$gene_id, expr.levels$ORF)]
    coordinates$GeneSize = ifelse(width(coordinates) <= 150, "Small", "Large")
    
    gene_size_quintile_split = quantile(width(coordinates), seq(0,1,0.2))
    gene_size_quintile = cut(width(coordinates), gene_size_quintile_split, include.lowest=TRUE)
    levels(gene_size_quintile) =  paste0("Q", 5:1)
    coordinates$GeneSizeQuintile = gene_size_quintile
    
    if(levelStyle=="UCSC") {
        seqlevelsStyle(coordinates) <- "UCSC"
    }
    
    return(coordinates)
}

get_chr_lengths <- function(levelStyle="ensembl") {
    # Retrieve chromosome lengths so we won't go over.
    chr.length = seqlengths(BSgenome.Scerevisiae.UCSC.sacCer3) - 1
    
    if(levelStyle=="ensembl") {
        names(chr.length) = gsub("^chr", "", names(chr.length))
        names(chr.length) = gsub("^M", "Mito", names(chr.length))
    }
    
    return(chr.length)
}