#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.1.2-beta
# Created on: 2019-03-11T09:05:43
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 1 job
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 2 jobs
#   samtools_view_filter: 2 jobs
#   picard_merge_sam_files: 1 job
#   picard_mark_duplicates: 2 jobs
#   macs2_callpeak: 3 jobs
#   TOTAL: 12 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6007406/efournie/Chaperone/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.base.ini,/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/Saccharomyces_cerevisiae.R64-1-1.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.Spt6_spt6-SA_39C_Cl3_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt6_spt6-SA_39C_Cl3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt6_spt6-SA_39C_Cl3_RS.426ebcede1f6b05a72756e0d329f476c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt6_spt6-SA_39C_Cl3_RS.426ebcede1f6b05a72756e0d329f476c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt6_spt6-SA_39C_Cl3 && \
`cat > trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4579.004.Index_15.MANU_Flag-spt6-SA_2h39C_CL3_R1.fastq.gz \
  trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.log
trimmomatic.Spt6_spt6-SA_39C_Cl3_RS.426ebcede1f6b05a72756e0d329f476c.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.1d8514c2cc65f858fd6e3011309371fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.1d8514c2cc65f858fd6e3011309371fb.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_dspt2Cl1	Chd1-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_dspt2Cl2	Chd1-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_spt6_39C_Cl1	Chd1-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_spt6_39C_Cl2	Chd1-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_Wt_39C_Cl1	Chd1-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_Wt_39C_Cl2	Chd1-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_WtCl1	Chd1-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_WtCl2	Chd1-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_ckII_Ctl_Cl2	Iws1_ckII_Ctl_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_ckII_Ctl_Cl3	Iws1_ckII_Ctl_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_spt6-SA_39C_Cl1/Iws1_spt6-SA_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_spt6-SA_39C_Cl1	Iws1_spt6-SA_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_spt6-SA_39C_Cl3/Iws1_spt6-SA_39C_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_spt6-SA_39C_Cl3	Iws1_spt6-SA_39C_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_WT_39C_Cl1	Iws1_WT_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_WT_39C_Cl3	Iws1_WT_39C_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_WT_Ctl_Cl1	Iws1_WT_Ctl_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1_WT_Ctl_Cl3	Iws1_WT_Ctl_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_dspt2Cl1	Iws1-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_dspt2Cl2	Iws1-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_spt6_39C_Cl1	Iws1-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_spt6_39C_Cl2	Iws1-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_Wt_39C_Cl1	Iws1-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_Wt_39C_Cl2	Iws1-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_WtCl1	Iws1-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_WtCl2	Iws1-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/No-TAG/No-TAG_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/No-TAG	No-TAG_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_spt6_39C_Cl1	Spt2-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_spt6_39C_Cl2	Spt2-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_Wt_39C_Cl1	Spt2-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_Wt_39C_Cl2	Spt2-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_ckII_Ctl_Cl2	Spt6_ckII_Ctl_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_ckII_Ctl_Cl3	Spt6_ckII_Ctl_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_spt6-SA_39C_Cl1/Spt6_spt6-SA_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_spt6-SA_39C_Cl1	Spt6_spt6-SA_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_spt6-SA_39C_Cl3	Spt6_spt6-SA_39C_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_WT_39C_Cl1	Spt6_WT_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_WT_39C_Cl3	Spt6_WT_39C_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_WT_Ctl_Cl1	Spt6_WT_Ctl_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6_WT_Ctl_Cl3	Spt6_WT_Ctl_Cl3_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_dspt2Cl1	Spt6-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_dspt2Cl2	Spt6-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_WtCl1	Spt6-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_WtCl2	Spt6-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.1d8514c2cc65f858fd6e3011309371fb.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.Spt6_spt6-SA_39C_Cl3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt6_spt6-SA_39C_Cl3_RS
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt6_spt6-SA_39C_Cl3_RS.b807ad07336a6fe35a02dd8f36a3be47.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt6_spt6-SA_39C_Cl3_RS.b807ad07336a6fe35a02dd8f36a3be47.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt6_spt6-SA_39C_Cl3_RS	SM:Spt6_spt6-SA_39C_Cl3	LB:Spt6_spt6-SA_39C_Cl3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS/Spt6_spt6-SA_39C_Cl3_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt6_spt6-SA_39C_Cl3_RS.b807ad07336a6fe35a02dd8f36a3be47.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Saccharomyces_cerevisiae" \
  --variable assembly="R64-1-1" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.Spt6_spt6-SA_39C_Cl3_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt6_spt6-SA_39C_Cl3_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt6_spt6-SA_39C_Cl3_RS.055a4136706e3c68b55ac9c23312f05a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt6_spt6-SA_39C_Cl3_RS.055a4136706e3c68b55ac9c23312f05a.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS/Spt6_spt6-SA_39C_Cl3_RS.sorted.bam \
  > alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3_RS/Spt6_spt6-SA_39C_Cl3_RS.sorted.filtered.bam
samtools_view_filter.Spt6_spt6-SA_39C_Cl3_RS.055a4136706e3c68b55ac9c23312f05a.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.Spt6_spt6-SA_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt6_spt6-SA_39C_Cl3
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt6_spt6-SA_39C_Cl3.d64f2d67968f81c7afdeaf3b6cb43f09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt6_spt6-SA_39C_Cl3.d64f2d67968f81c7afdeaf3b6cb43f09.mugqic.done'
mkdir -p alignment/Spt6_spt6-SA_39C_Cl3 && \
ln -s -f Spt6_spt6-SA_39C_Cl3_RS/Spt6_spt6-SA_39C_Cl3_RS.sorted.filtered.bam alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.merged.bam
symlink_readset_sample_bam.Spt6_spt6-SA_39C_Cl3.d64f2d67968f81c7afdeaf3b6cb43f09.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.Spt6_spt6-SA_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Spt6_spt6-SA_39C_Cl3
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Spt6_spt6-SA_39C_Cl3.6556ea720c6f7f50527dd12998315e92.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Spt6_spt6-SA_39C_Cl3.6556ea720c6f7f50527dd12998315e92.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.merged.bam \
 OUTPUT=alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.sorted.dup.bam \
 METRICS_FILE=alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Spt6_spt6-SA_39C_Cl3.6556ea720c6f7f50527dd12998315e92.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.Spt6_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_spt6_39C_Cl3
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_spt6_39C_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3 \
  >& peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3.diag.macs.out
macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak > peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in Chd1-Myc_dspt2Cl2 Chd1-Myc_dspt2Cl1 Iws1-Myc_dspt2Cl2 Iws1-Myc_dspt2Cl1 Spt6-Myc_dspt2Cl2 Spt6-Myc_dspt2Cl1 Chd1-Myc_spt6_39C_Cl2 Chd1-Myc_spt6_39C_Cl1 Iws1-Myc_spt6_39C_Cl2 Iws1-Myc_spt6_39C_Cl1 Spt2-Myc_spt6_39C_Cl2 Spt2-Myc_spt6_39C_Cl1 Chd1-Myc_Wt_39C_Cl2 Chd1-Myc_Wt_39C_Cl1 Iws1-Myc_Wt_39C_Cl2 Iws1-Myc_Wt_39C_Cl1 Spt2-Myc_Wt_39C_Cl2 Spt2-Myc_Wt_39C_Cl1 Chd1-Myc_WtCl2 Chd1-Myc_WtCl1 Iws1-Myc_WtCl2 Iws1-Myc_WtCl1 Spt6-Myc_WtCl2 Spt6-Myc_WtCl1 Spt6_WT_39C_Cl3 Spt6_ckII_Ctl_Cl2 Spt6_spt6_39C_Cl3 Spt6_ckII_Ctl_Cl3 Spt6_WT_Ctl_Cl3 Spt6_WT_39C_Cl1 Spt6_spt6_39C_Cl1 Spt6_WT_Ctl_Cl1 Iws1_WT_Ctl_Cl1 Iws1_WT_Ctl_Cl3 Iws1_WT_39C_Cl1 Iws1_ckII_Ctl_Cl2 Iws1_ckII_Ctl_Cl3 Iws1_WT_39C_Cl3 Iws1_spt6_39C_Cl3 Iws1_spt6_39C_Cl1
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
LOG_MD5=$(echo $USER-'206.12.124.2-ChipSeq-Iws1-Myc_dspt2Cl2.Iws1-Myc_dspt2Cl2_RS,Iws1-Myc_dspt2Cl1.Iws1-Myc_dspt2Cl1_RS,Chd1-Myc_WtCl1.Chd1-Myc_WtCl1_RS,Chd1-Myc_WtCl2.Chd1-Myc_WtCl2_RS,Spt2-Myc_spt6_39C_Cl2.Spt2-Myc_spt6_39C_Cl2_RS,Spt6-Myc_dspt2Cl2.Spt6-Myc_dspt2Cl2_RS,Spt6-Myc_dspt2Cl1.Spt6-Myc_dspt2Cl1_RS,Spt2-Myc_spt6_39C_Cl1.Spt2-Myc_spt6_39C_Cl1_RS,Chd1-Myc_dspt2Cl2.Chd1-Myc_dspt2Cl2_RS,Iws1_ckII_Ctl_Cl2.Iws1_ckII_Ctl_Cl2_RS,Iws1_ckII_Ctl_Cl3.Iws1_ckII_Ctl_Cl3_RS,Iws1-Myc_WtCl2.Iws1-Myc_WtCl2_RS,Iws1_spt6-SA_39C_Cl1.Iws1_spt6-SA_39C_Cl1_RS,Spt6_WT_39C_Cl1.Spt6_WT_39C_Cl1_RS,Iws1_spt6-SA_39C_Cl3.Iws1_spt6-SA_39C_Cl3_RS,Spt2-Myc_Wt_39C_Cl1.Spt2-Myc_Wt_39C_Cl1_RS,No-TAG.No-TAG_RS,Iws1_WT_39C_Cl1.Iws1_WT_39C_Cl1_RS,Iws1_WT_39C_Cl3.Iws1_WT_39C_Cl3_RS,Spt6_spt6-SA_39C_Cl1.Spt6_spt6-SA_39C_Cl1_RS,Chd1-Myc_spt6_39C_Cl2.Chd1-Myc_spt6_39C_Cl2_RS,Chd1-Myc_spt6_39C_Cl1.Chd1-Myc_spt6_39C_Cl1_RS,Spt6-Myc_WtCl2.Spt6-Myc_WtCl2_RS,Iws1-Myc_spt6_39C_Cl2.Iws1-Myc_spt6_39C_Cl2_RS,Iws1-Myc_spt6_39C_Cl1.Iws1-Myc_spt6_39C_Cl1_RS,Spt6_ckII_Ctl_Cl3.Spt6_ckII_Ctl_Cl3_RS,Spt2-Myc_Wt_39C_Cl2.Spt2-Myc_Wt_39C_Cl2_RS,Spt6-Myc_WtCl1.Spt6-Myc_WtCl1_RS,Chd1-Myc_Wt_39C_Cl2.Chd1-Myc_Wt_39C_Cl2_RS,Chd1-Myc_Wt_39C_Cl1.Chd1-Myc_Wt_39C_Cl1_RS,Spt6_WT_Ctl_Cl1.Spt6_WT_Ctl_Cl1_RS,Iws1_WT_Ctl_Cl1.Iws1_WT_Ctl_Cl1_RS,Iws1-Myc_WtCl1.Iws1-Myc_WtCl1_RS,Iws1_WT_Ctl_Cl3.Iws1_WT_Ctl_Cl3_RS,Chd1-Myc_dspt2Cl1.Chd1-Myc_dspt2Cl1_RS,Spt6_spt6-SA_39C_Cl3.Spt6_spt6-SA_39C_Cl3_RS,Spt6_ckII_Ctl_Cl2.Spt6_ckII_Ctl_Cl2_RS,Spt6_WT_39C_Cl3.Spt6_WT_39C_Cl3_RS,Spt6_WT_Ctl_Cl3.Spt6_WT_Ctl_Cl3_RS,Iws1-Myc_Wt_39C_Cl2.Iws1-Myc_Wt_39C_Cl2_RS,Iws1-Myc_Wt_39C_Cl1.Iws1-Myc_Wt_39C_Cl1_RS' | md5sum | awk '{ print $1 }')
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,macs2_callpeak&samples=41&md5=$LOG_MD5" --quiet --output-document=/dev/null

