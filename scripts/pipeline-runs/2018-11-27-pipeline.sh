#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.1.2-beta
# Created on: 2018-11-27T14:15:18
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   bwa_mem_picard_sort_sam: 0 job... skipping
#   samtools_view_filter: 0 job... skipping
#   picard_merge_sam_files: 0 job... skipping
#   picard_mark_duplicates: 0 job... skipping
#   macs2_callpeak: 81 jobs
#   TOTAL: 81 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6007406/efournie/Chaperone/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.base.ini,/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/Saccharomyces_cerevisiae.R64-1-1.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.Chd1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_dspt2Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_dspt2Cl2.82c1a7a378a70a79b83a63ee0cdb8f23.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_dspt2Cl2.82c1a7a378a70a79b83a63ee0cdb8f23.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_dspt2Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2 \
  >& peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2.diag.macs.out
macs2_callpeak.Chd1-Myc_dspt2Cl2.82c1a7a378a70a79b83a63ee0cdb8f23.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl2
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl2.ae46a26ff9a8b4136c7030695eee4f85.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl2.ae46a26ff9a8b4136c7030695eee4f85.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_peaks.narrowPeak > peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl2.ae46a26ff9a8b4136c7030695eee4f85.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.Chd1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_dspt2Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_dspt2Cl1.9441726e716ec730573c13628f291808.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_dspt2Cl1.9441726e716ec730573c13628f291808.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_dspt2Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1 \
  >& peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1.diag.macs.out
macs2_callpeak.Chd1-Myc_dspt2Cl1.9441726e716ec730573c13628f291808.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl1
JOB_DEPENDENCIES=$macs2_callpeak_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl1.3f86c06f1a30f901b18b2c657928ab4a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl1.3f86c06f1a30f901b18b2c657928ab4a.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_peaks.narrowPeak > peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_dspt2Cl1.3f86c06f1a30f901b18b2c657928ab4a.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.Iws1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_dspt2Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_dspt2Cl2.cf8c0b772c381cc8d4cae4ab24a745df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_dspt2Cl2.cf8c0b772c381cc8d4cae4ab24a745df.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_dspt2Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2 \
  >& peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2.diag.macs.out
macs2_callpeak.Iws1-Myc_dspt2Cl2.cf8c0b772c381cc8d4cae4ab24a745df.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl2
JOB_DEPENDENCIES=$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl2.fee57d67c35a487092ee0ac4100f1888.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl2.fee57d67c35a487092ee0ac4100f1888.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_peaks.narrowPeak > peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl2.fee57d67c35a487092ee0ac4100f1888.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_7_JOB_ID: macs2_callpeak.Iws1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_dspt2Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_dspt2Cl1.f22ee867d5325da52e4c5f4e66d51408.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_dspt2Cl1.f22ee867d5325da52e4c5f4e66d51408.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_dspt2Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1 \
  >& peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1.diag.macs.out
macs2_callpeak.Iws1-Myc_dspt2Cl1.f22ee867d5325da52e4c5f4e66d51408.mugqic.done
)
macs2_callpeak_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_8_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl1
JOB_DEPENDENCIES=$macs2_callpeak_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl1.b05f87d6a4455e7858dea09fc1d3d30f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl1.b05f87d6a4455e7858dea09fc1d3d30f.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_peaks.narrowPeak > peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_dspt2Cl1.b05f87d6a4455e7858dea09fc1d3d30f.mugqic.done
)
macs2_callpeak_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_9_JOB_ID: macs2_callpeak.Spt6-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6-Myc_dspt2Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6-Myc_dspt2Cl2.20ef78883bd1ea9a26aba6da52b02abb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6-Myc_dspt2Cl2.20ef78883bd1ea9a26aba6da52b02abb.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6-Myc_dspt2Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2 \
  >& peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2.diag.macs.out
macs2_callpeak.Spt6-Myc_dspt2Cl2.20ef78883bd1ea9a26aba6da52b02abb.mugqic.done
)
macs2_callpeak_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_10_JOB_ID: macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl2
JOB_DEPENDENCIES=$macs2_callpeak_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl2.beff3ab933a6e4dfdbc460cb0f498c8b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl2.beff3ab933a6e4dfdbc460cb0f498c8b.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_peaks.narrowPeak > peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl2.beff3ab933a6e4dfdbc460cb0f498c8b.mugqic.done
)
macs2_callpeak_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_11_JOB_ID: macs2_callpeak.Spt6-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6-Myc_dspt2Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6-Myc_dspt2Cl1.095bbf9e83498392d0ca0845b92d912d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6-Myc_dspt2Cl1.095bbf9e83498392d0ca0845b92d912d.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6-Myc_dspt2Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1 \
  >& peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1.diag.macs.out
macs2_callpeak.Spt6-Myc_dspt2Cl1.095bbf9e83498392d0ca0845b92d912d.mugqic.done
)
macs2_callpeak_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_12_JOB_ID: macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl1
JOB_DEPENDENCIES=$macs2_callpeak_11_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl1.822c18e48f49c84925227be88fc94cc0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl1.822c18e48f49c84925227be88fc94cc0.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_peaks.narrowPeak > peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6-Myc_dspt2Cl1.822c18e48f49c84925227be88fc94cc0.mugqic.done
)
macs2_callpeak_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_13_JOB_ID: macs2_callpeak.Chd1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_spt6_39C_Cl2.a1355d4faeedb8db71fb84c39074eecd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_spt6_39C_Cl2.a1355d4faeedb8db71fb84c39074eecd.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_spt6_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2 \
  >& peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2.diag.macs.out
macs2_callpeak.Chd1-Myc_spt6_39C_Cl2.a1355d4faeedb8db71fb84c39074eecd.mugqic.done
)
macs2_callpeak_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_14_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_13_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl2.c1d0a4ed6bcfe3671e3f28280be344b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl2.c1d0a4ed6bcfe3671e3f28280be344b5.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_peaks.narrowPeak > peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl2.c1d0a4ed6bcfe3671e3f28280be344b5.mugqic.done
)
macs2_callpeak_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_15_JOB_ID: macs2_callpeak.Chd1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_spt6_39C_Cl1.1ba1e6226540277166e94ba83f18f1e6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_spt6_39C_Cl1.1ba1e6226540277166e94ba83f18f1e6.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_spt6_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1 \
  >& peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1.diag.macs.out
macs2_callpeak.Chd1-Myc_spt6_39C_Cl1.1ba1e6226540277166e94ba83f18f1e6.mugqic.done
)
macs2_callpeak_15_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_16_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_15_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl1.32eec42266db75a2e44098d3ce2fce86.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl1.32eec42266db75a2e44098d3ce2fce86.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_peaks.narrowPeak > peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_spt6_39C_Cl1.32eec42266db75a2e44098d3ce2fce86.mugqic.done
)
macs2_callpeak_16_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_17_JOB_ID: macs2_callpeak.Iws1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_spt6_39C_Cl2.6d05091230998c0e3ca3a76817740beb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_spt6_39C_Cl2.6d05091230998c0e3ca3a76817740beb.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_spt6_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2 \
  >& peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2.diag.macs.out
macs2_callpeak.Iws1-Myc_spt6_39C_Cl2.6d05091230998c0e3ca3a76817740beb.mugqic.done
)
macs2_callpeak_17_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_18_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_17_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl2.f4d3ff5ace859262162ef0b9d7086f33.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl2.f4d3ff5ace859262162ef0b9d7086f33.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_peaks.narrowPeak > peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl2.f4d3ff5ace859262162ef0b9d7086f33.mugqic.done
)
macs2_callpeak_18_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_19_JOB_ID: macs2_callpeak.Iws1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_spt6_39C_Cl1.ae7480c4a2dce0aea5459f320178f69c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_spt6_39C_Cl1.ae7480c4a2dce0aea5459f320178f69c.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_spt6_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1 \
  >& peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1.diag.macs.out
macs2_callpeak.Iws1-Myc_spt6_39C_Cl1.ae7480c4a2dce0aea5459f320178f69c.mugqic.done
)
macs2_callpeak_19_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_20_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_19_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl1.a929149f32418e9db8c6264b4c34fe3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl1.a929149f32418e9db8c6264b4c34fe3a.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_peaks.narrowPeak > peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_spt6_39C_Cl1.a929149f32418e9db8c6264b4c34fe3a.mugqic.done
)
macs2_callpeak_20_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_21_JOB_ID: macs2_callpeak.Spt2-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt2-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt2-Myc_spt6_39C_Cl2.07126f64f290ab497e0b6e072d6149c8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt2-Myc_spt6_39C_Cl2.07126f64f290ab497e0b6e072d6149c8.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt2-Myc_spt6_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2 \
  >& peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2.diag.macs.out
macs2_callpeak.Spt2-Myc_spt6_39C_Cl2.07126f64f290ab497e0b6e072d6149c8.mugqic.done
)
macs2_callpeak_21_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_22_JOB_ID: macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_21_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl2.dd5276d976eada78b14f05e121a2b90e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl2.dd5276d976eada78b14f05e121a2b90e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_peaks.narrowPeak > peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl2.dd5276d976eada78b14f05e121a2b90e.mugqic.done
)
macs2_callpeak_22_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_23_JOB_ID: macs2_callpeak.Spt2-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt2-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt2-Myc_spt6_39C_Cl1.5cf5d9f36f042afebceaa0297de0f2e9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt2-Myc_spt6_39C_Cl1.5cf5d9f36f042afebceaa0297de0f2e9.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt2-Myc_spt6_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1 \
  >& peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1.diag.macs.out
macs2_callpeak.Spt2-Myc_spt6_39C_Cl1.5cf5d9f36f042afebceaa0297de0f2e9.mugqic.done
)
macs2_callpeak_23_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_24_JOB_ID: macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_23_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl1.79ee385d90bec22d7663d8b4f1d1e533.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl1.79ee385d90bec22d7663d8b4f1d1e533.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_peaks.narrowPeak > peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt2-Myc_spt6_39C_Cl1.79ee385d90bec22d7663d8b4f1d1e533.mugqic.done
)
macs2_callpeak_24_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_25_JOB_ID: macs2_callpeak.Chd1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_Wt_39C_Cl2.720ed835bfb2f4b40c0070d33997983d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_Wt_39C_Cl2.720ed835bfb2f4b40c0070d33997983d.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_Wt_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2 \
  >& peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2.diag.macs.out
macs2_callpeak.Chd1-Myc_Wt_39C_Cl2.720ed835bfb2f4b40c0070d33997983d.mugqic.done
)
macs2_callpeak_25_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_26_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_25_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl2.e2c3b686201a18cdc51363745cf53fc9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl2.e2c3b686201a18cdc51363745cf53fc9.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_peaks.narrowPeak > peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl2.e2c3b686201a18cdc51363745cf53fc9.mugqic.done
)
macs2_callpeak_26_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_27_JOB_ID: macs2_callpeak.Chd1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_Wt_39C_Cl1.6c90abbea6e134943c9ca778bc596e40.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_Wt_39C_Cl1.6c90abbea6e134943c9ca778bc596e40.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_Wt_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1 \
  >& peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1.diag.macs.out
macs2_callpeak.Chd1-Myc_Wt_39C_Cl1.6c90abbea6e134943c9ca778bc596e40.mugqic.done
)
macs2_callpeak_27_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_28_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_27_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl1.d24a0ba772e0d2dab2a378bba48cb464.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl1.d24a0ba772e0d2dab2a378bba48cb464.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_peaks.narrowPeak > peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_Wt_39C_Cl1.d24a0ba772e0d2dab2a378bba48cb464.mugqic.done
)
macs2_callpeak_28_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_29_JOB_ID: macs2_callpeak.Iws1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_Wt_39C_Cl2.82c3223752b334302202678041a55eb0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_Wt_39C_Cl2.82c3223752b334302202678041a55eb0.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_Wt_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2 \
  >& peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2.diag.macs.out
macs2_callpeak.Iws1-Myc_Wt_39C_Cl2.82c3223752b334302202678041a55eb0.mugqic.done
)
macs2_callpeak_29_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_30_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_29_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl2.f24853a893deca0d2ee10323fc8abd8e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl2.f24853a893deca0d2ee10323fc8abd8e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_peaks.narrowPeak > peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl2.f24853a893deca0d2ee10323fc8abd8e.mugqic.done
)
macs2_callpeak_30_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_31_JOB_ID: macs2_callpeak.Iws1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_Wt_39C_Cl1.be123f0e34d69ccbaac1432fc5598ffc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_Wt_39C_Cl1.be123f0e34d69ccbaac1432fc5598ffc.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_Wt_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1 \
  >& peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1.diag.macs.out
macs2_callpeak.Iws1-Myc_Wt_39C_Cl1.be123f0e34d69ccbaac1432fc5598ffc.mugqic.done
)
macs2_callpeak_31_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_32_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_31_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl1.5046aa480b5cdfbcfaf680bb18c0c7bb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl1.5046aa480b5cdfbcfaf680bb18c0c7bb.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_peaks.narrowPeak > peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_Wt_39C_Cl1.5046aa480b5cdfbcfaf680bb18c0c7bb.mugqic.done
)
macs2_callpeak_32_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_33_JOB_ID: macs2_callpeak.Spt2-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt2-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt2-Myc_Wt_39C_Cl2.6492f8da6bf4f54041c1f8a0ae995a4d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt2-Myc_Wt_39C_Cl2.6492f8da6bf4f54041c1f8a0ae995a4d.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt2-Myc_Wt_39C_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2 \
  >& peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2.diag.macs.out
macs2_callpeak.Spt2-Myc_Wt_39C_Cl2.6492f8da6bf4f54041c1f8a0ae995a4d.mugqic.done
)
macs2_callpeak_33_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_34_JOB_ID: macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_33_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl2.7554289222bd167b6d6cd6ab8b733b5f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl2.7554289222bd167b6d6cd6ab8b733b5f.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_peaks.narrowPeak > peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl2.7554289222bd167b6d6cd6ab8b733b5f.mugqic.done
)
macs2_callpeak_34_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_35_JOB_ID: macs2_callpeak.Spt2-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt2-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt2-Myc_Wt_39C_Cl1.064a60518be435c962daf22750c019fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt2-Myc_Wt_39C_Cl1.064a60518be435c962daf22750c019fd.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt2-Myc_Wt_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1 \
  >& peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1.diag.macs.out
macs2_callpeak.Spt2-Myc_Wt_39C_Cl1.064a60518be435c962daf22750c019fd.mugqic.done
)
macs2_callpeak_35_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_36_JOB_ID: macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_35_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl1.ee005f4ceacb87a6e605fe47e38ceddd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl1.ee005f4ceacb87a6e605fe47e38ceddd.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_peaks.narrowPeak > peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt2-Myc_Wt_39C_Cl1.ee005f4ceacb87a6e605fe47e38ceddd.mugqic.done
)
macs2_callpeak_36_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_37_JOB_ID: macs2_callpeak.Chd1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_WtCl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_WtCl2.73792d46b14b48d42da83bf4a87370b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_WtCl2.73792d46b14b48d42da83bf4a87370b4.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_WtCl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2 \
  >& peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2.diag.macs.out
macs2_callpeak.Chd1-Myc_WtCl2.73792d46b14b48d42da83bf4a87370b4.mugqic.done
)
macs2_callpeak_37_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_38_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_WtCl2
JOB_DEPENDENCIES=$macs2_callpeak_37_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_WtCl2.9a37e2db6cb952ee9cd0b425b2020063.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_WtCl2.9a37e2db6cb952ee9cd0b425b2020063.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_peaks.narrowPeak > peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_WtCl2.9a37e2db6cb952ee9cd0b425b2020063.mugqic.done
)
macs2_callpeak_38_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_39_JOB_ID: macs2_callpeak.Chd1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Chd1-Myc_WtCl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Chd1-Myc_WtCl1.a4b8b1a2db66cd30ffffecd3c1cd8993.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Chd1-Myc_WtCl1.a4b8b1a2db66cd30ffffecd3c1cd8993.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Chd1-Myc_WtCl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1 \
  >& peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1.diag.macs.out
macs2_callpeak.Chd1-Myc_WtCl1.a4b8b1a2db66cd30ffffecd3c1cd8993.mugqic.done
)
macs2_callpeak_39_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_40_JOB_ID: macs2_callpeak_bigBed.Chd1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Chd1-Myc_WtCl1
JOB_DEPENDENCIES=$macs2_callpeak_39_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Chd1-Myc_WtCl1.ba37c7780af6debd54ba56cf0800af3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Chd1-Myc_WtCl1.ba37c7780af6debd54ba56cf0800af3a.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_peaks.narrowPeak > peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Chd1-Myc_WtCl1.ba37c7780af6debd54ba56cf0800af3a.mugqic.done
)
macs2_callpeak_40_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_41_JOB_ID: macs2_callpeak.Iws1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_WtCl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_WtCl2.1c630392ea73c4b83a94ee44f0a24758.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_WtCl2.1c630392ea73c4b83a94ee44f0a24758.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_WtCl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2 \
  >& peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2.diag.macs.out
macs2_callpeak.Iws1-Myc_WtCl2.1c630392ea73c4b83a94ee44f0a24758.mugqic.done
)
macs2_callpeak_41_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_42_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_WtCl2
JOB_DEPENDENCIES=$macs2_callpeak_41_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_WtCl2.05a7dfb652a9074138e81f35dda32de1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_WtCl2.05a7dfb652a9074138e81f35dda32de1.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_peaks.narrowPeak > peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_WtCl2.05a7dfb652a9074138e81f35dda32de1.mugqic.done
)
macs2_callpeak_42_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_43_JOB_ID: macs2_callpeak.Iws1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1-Myc_WtCl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1-Myc_WtCl1.f74cd77f208a2b71d7bdcfe1ac1a6ed8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1-Myc_WtCl1.f74cd77f208a2b71d7bdcfe1ac1a6ed8.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1-Myc_WtCl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1 \
  >& peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1.diag.macs.out
macs2_callpeak.Iws1-Myc_WtCl1.f74cd77f208a2b71d7bdcfe1ac1a6ed8.mugqic.done
)
macs2_callpeak_43_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_44_JOB_ID: macs2_callpeak_bigBed.Iws1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1-Myc_WtCl1
JOB_DEPENDENCIES=$macs2_callpeak_43_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1-Myc_WtCl1.ea254cc52d85e44fd4a6295d5cba0e02.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1-Myc_WtCl1.ea254cc52d85e44fd4a6295d5cba0e02.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_peaks.narrowPeak > peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1-Myc_WtCl1.ea254cc52d85e44fd4a6295d5cba0e02.mugqic.done
)
macs2_callpeak_44_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_45_JOB_ID: macs2_callpeak.Spt6-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6-Myc_WtCl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6-Myc_WtCl2.f3d4ed0ec8c2140a9fc11618afbb98ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6-Myc_WtCl2.f3d4ed0ec8c2140a9fc11618afbb98ba.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6-Myc_WtCl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2 \
  >& peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2.diag.macs.out
macs2_callpeak.Spt6-Myc_WtCl2.f3d4ed0ec8c2140a9fc11618afbb98ba.mugqic.done
)
macs2_callpeak_45_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_46_JOB_ID: macs2_callpeak_bigBed.Spt6-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6-Myc_WtCl2
JOB_DEPENDENCIES=$macs2_callpeak_45_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6-Myc_WtCl2.1176cda1944727771a3635980f61e8a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6-Myc_WtCl2.1176cda1944727771a3635980f61e8a8.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_peaks.narrowPeak > peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6-Myc_WtCl2.1176cda1944727771a3635980f61e8a8.mugqic.done
)
macs2_callpeak_46_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_47_JOB_ID: macs2_callpeak.Spt6-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6-Myc_WtCl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6-Myc_WtCl1.89b008d4215f9f16878658ce00cd1cfb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6-Myc_WtCl1.89b008d4215f9f16878658ce00cd1cfb.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6-Myc_WtCl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1 \
  >& peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1.diag.macs.out
macs2_callpeak.Spt6-Myc_WtCl1.89b008d4215f9f16878658ce00cd1cfb.mugqic.done
)
macs2_callpeak_47_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_48_JOB_ID: macs2_callpeak_bigBed.Spt6-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6-Myc_WtCl1
JOB_DEPENDENCIES=$macs2_callpeak_47_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6-Myc_WtCl1.7cc0e1677c87914f81bffc7c99392fcb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6-Myc_WtCl1.7cc0e1677c87914f81bffc7c99392fcb.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_peaks.narrowPeak > peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6-Myc_WtCl1.7cc0e1677c87914f81bffc7c99392fcb.mugqic.done
)
macs2_callpeak_48_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_49_JOB_ID: macs2_callpeak.Spt6_WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_WT_39C_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_WT_39C_Cl3.29a575e189e9c46e26aa312aaeb21a35.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_WT_39C_Cl3.29a575e189e9c46e26aa312aaeb21a35.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_WT_39C_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3 \
  >& peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3.diag.macs.out
macs2_callpeak.Spt6_WT_39C_Cl3.29a575e189e9c46e26aa312aaeb21a35.mugqic.done
)
macs2_callpeak_49_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_49_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_50_JOB_ID: macs2_callpeak_bigBed.Spt6_WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_WT_39C_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_49_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_WT_39C_Cl3.6c6a7e929fb0d04029a669c96e213801.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_WT_39C_Cl3.6c6a7e929fb0d04029a669c96e213801.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3_peaks.narrowPeak > peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_WT_39C_Cl3/Spt6_WT_39C_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_WT_39C_Cl3.6c6a7e929fb0d04029a669c96e213801.mugqic.done
)
macs2_callpeak_50_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_50_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_51_JOB_ID: macs2_callpeak.Spt6_ckII_Ctl_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_ckII_Ctl_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_ckII_Ctl_Cl2.482c476b1e444abc6399209b84003156.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_ckII_Ctl_Cl2.482c476b1e444abc6399209b84003156.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_ckII_Ctl_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2 \
  >& peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2.diag.macs.out
macs2_callpeak.Spt6_ckII_Ctl_Cl2.482c476b1e444abc6399209b84003156.mugqic.done
)
macs2_callpeak_51_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_51_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_52_JOB_ID: macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_51_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl2.cf8b239187279a1ce261eb0851ad790e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl2.cf8b239187279a1ce261eb0851ad790e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2_peaks.narrowPeak > peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_ckII_Ctl_Cl2/Spt6_ckII_Ctl_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl2.cf8b239187279a1ce261eb0851ad790e.mugqic.done
)
macs2_callpeak_52_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_52_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_53_JOB_ID: macs2_callpeak.Spt6_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_spt6_39C_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_spt6_39C_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_spt6-SA_39C_Cl3/Spt6_spt6-SA_39C_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3 \
  >& peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3.diag.macs.out
macs2_callpeak.Spt6_spt6_39C_Cl3.af3954c5f8421ae153286afe6c95f0fd.mugqic.done
)
macs2_callpeak_53_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_53_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_54_JOB_ID: macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_53_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak > peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_spt6_39C_Cl3/Spt6_spt6_39C_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_spt6_39C_Cl3.264ef3f475dadfa55f5fa4b46a65a72f.mugqic.done
)
macs2_callpeak_54_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_54_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_55_JOB_ID: macs2_callpeak.Spt6_ckII_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_ckII_Ctl_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_ckII_Ctl_Cl3.b7236309af4179046e2c4d9a103354d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_ckII_Ctl_Cl3.b7236309af4179046e2c4d9a103354d1.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_ckII_Ctl_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3 \
  >& peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3.diag.macs.out
macs2_callpeak.Spt6_ckII_Ctl_Cl3.b7236309af4179046e2c4d9a103354d1.mugqic.done
)
macs2_callpeak_55_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_55_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_56_JOB_ID: macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_55_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl3.2d72939edb82b3c693668e8b21d4b8bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl3.2d72939edb82b3c693668e8b21d4b8bc.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3_peaks.narrowPeak > peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_ckII_Ctl_Cl3/Spt6_ckII_Ctl_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_ckII_Ctl_Cl3.2d72939edb82b3c693668e8b21d4b8bc.mugqic.done
)
macs2_callpeak_56_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_56_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_57_JOB_ID: macs2_callpeak.Spt6_WT_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_WT_Ctl_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_WT_Ctl_Cl3.14ca94c41e0b5818339d46fabc92f46d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_WT_Ctl_Cl3.14ca94c41e0b5818339d46fabc92f46d.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_WT_Ctl_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3 \
  >& peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3.diag.macs.out
macs2_callpeak.Spt6_WT_Ctl_Cl3.14ca94c41e0b5818339d46fabc92f46d.mugqic.done
)
macs2_callpeak_57_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_57_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_58_JOB_ID: macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_57_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl3.2e05d5922112d2541f86af4efcb81795.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl3.2e05d5922112d2541f86af4efcb81795.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3_peaks.narrowPeak > peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_WT_Ctl_Cl3/Spt6_WT_Ctl_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl3.2e05d5922112d2541f86af4efcb81795.mugqic.done
)
macs2_callpeak_58_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_58_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_59_JOB_ID: macs2_callpeak.Spt6_WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_WT_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_WT_39C_Cl1.f6111bd04211eb5a3f83416feb29b667.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_WT_39C_Cl1.f6111bd04211eb5a3f83416feb29b667.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_WT_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1 \
  >& peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1.diag.macs.out
macs2_callpeak.Spt6_WT_39C_Cl1.f6111bd04211eb5a3f83416feb29b667.mugqic.done
)
macs2_callpeak_59_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_59_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_60_JOB_ID: macs2_callpeak_bigBed.Spt6_WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_WT_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_59_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_WT_39C_Cl1.4e7ec0ae2c241842a06a963c743b1faa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_WT_39C_Cl1.4e7ec0ae2c241842a06a963c743b1faa.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1_peaks.narrowPeak > peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_WT_39C_Cl1/Spt6_WT_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_WT_39C_Cl1.4e7ec0ae2c241842a06a963c743b1faa.mugqic.done
)
macs2_callpeak_60_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_60_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_61_JOB_ID: macs2_callpeak.Spt6_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_spt6_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_spt6_39C_Cl1.190b57ace448a39f067e61abcac5cc54.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_spt6_39C_Cl1.190b57ace448a39f067e61abcac5cc54.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_spt6_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_spt6-SA_39C_Cl1/Spt6_spt6-SA_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1 \
  >& peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1.diag.macs.out
macs2_callpeak.Spt6_spt6_39C_Cl1.190b57ace448a39f067e61abcac5cc54.mugqic.done
)
macs2_callpeak_61_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_61_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_62_JOB_ID: macs2_callpeak_bigBed.Spt6_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_spt6_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_61_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_spt6_39C_Cl1.f602175dd1d749dcec75f8e3065d654a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_spt6_39C_Cl1.f602175dd1d749dcec75f8e3065d654a.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1_peaks.narrowPeak > peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_spt6_39C_Cl1/Spt6_spt6_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_spt6_39C_Cl1.f602175dd1d749dcec75f8e3065d654a.mugqic.done
)
macs2_callpeak_62_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_62_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_63_JOB_ID: macs2_callpeak.Spt6_WT_Ctl_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Spt6_WT_Ctl_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Spt6_WT_Ctl_Cl1.e36117a6f7073be5cc338f0a2a2a69c2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Spt6_WT_Ctl_Cl1.e36117a6f7073be5cc338f0a2a2a69c2.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Spt6_WT_Ctl_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1 \
  >& peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1.diag.macs.out
macs2_callpeak.Spt6_WT_Ctl_Cl1.e36117a6f7073be5cc338f0a2a2a69c2.mugqic.done
)
macs2_callpeak_63_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_63_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_64_JOB_ID: macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_63_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl1.28593f1e5ee9f8468b0e9aedc4f3c488.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl1.28593f1e5ee9f8468b0e9aedc4f3c488.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1_peaks.narrowPeak > peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Spt6_WT_Ctl_Cl1/Spt6_WT_Ctl_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Spt6_WT_Ctl_Cl1.28593f1e5ee9f8468b0e9aedc4f3c488.mugqic.done
)
macs2_callpeak_64_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_64_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_65_JOB_ID: macs2_callpeak.Iws1_WT_Ctl_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_WT_Ctl_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_WT_Ctl_Cl1.613c0436e12b2997dc2ee7b9d887f26a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_WT_Ctl_Cl1.613c0436e12b2997dc2ee7b9d887f26a.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_WT_Ctl_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1 \
  >& peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1.diag.macs.out
macs2_callpeak.Iws1_WT_Ctl_Cl1.613c0436e12b2997dc2ee7b9d887f26a.mugqic.done
)
macs2_callpeak_65_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_65_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_66_JOB_ID: macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_65_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl1.3b7d11126772fd922e85db84a3199151.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl1.3b7d11126772fd922e85db84a3199151.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1_peaks.narrowPeak > peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_WT_Ctl_Cl1/Iws1_WT_Ctl_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl1.3b7d11126772fd922e85db84a3199151.mugqic.done
)
macs2_callpeak_66_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_66_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_67_JOB_ID: macs2_callpeak.Iws1_WT_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_WT_Ctl_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_WT_Ctl_Cl3.09c2efb7da121b0d8da9ee9f28c886ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_WT_Ctl_Cl3.09c2efb7da121b0d8da9ee9f28c886ba.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_WT_Ctl_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3 \
  >& peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3.diag.macs.out
macs2_callpeak.Iws1_WT_Ctl_Cl3.09c2efb7da121b0d8da9ee9f28c886ba.mugqic.done
)
macs2_callpeak_67_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_67_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_68_JOB_ID: macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_67_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl3.e140ad7cf3b465e3adbbf7353f43fc5e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl3.e140ad7cf3b465e3adbbf7353f43fc5e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3_peaks.narrowPeak > peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_WT_Ctl_Cl3/Iws1_WT_Ctl_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_WT_Ctl_Cl3.e140ad7cf3b465e3adbbf7353f43fc5e.mugqic.done
)
macs2_callpeak_68_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_68_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_69_JOB_ID: macs2_callpeak.Iws1_WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_WT_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_WT_39C_Cl1.1df4785c7d4495e0889b6be28793d8ef.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_WT_39C_Cl1.1df4785c7d4495e0889b6be28793d8ef.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_WT_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1 \
  >& peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1.diag.macs.out
macs2_callpeak.Iws1_WT_39C_Cl1.1df4785c7d4495e0889b6be28793d8ef.mugqic.done
)
macs2_callpeak_69_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_69_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_70_JOB_ID: macs2_callpeak_bigBed.Iws1_WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_WT_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_69_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_WT_39C_Cl1.06a92eddf2ec5810f242e8b5618e5b06.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_WT_39C_Cl1.06a92eddf2ec5810f242e8b5618e5b06.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1_peaks.narrowPeak > peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_WT_39C_Cl1/Iws1_WT_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_WT_39C_Cl1.06a92eddf2ec5810f242e8b5618e5b06.mugqic.done
)
macs2_callpeak_70_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_70_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_71_JOB_ID: macs2_callpeak.Iws1_ckII_Ctl_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_ckII_Ctl_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_ckII_Ctl_Cl2.8962083f4947faac8cf045da0487376c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_ckII_Ctl_Cl2.8962083f4947faac8cf045da0487376c.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_ckII_Ctl_Cl2 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2 \
  >& peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2.diag.macs.out
macs2_callpeak.Iws1_ckII_Ctl_Cl2.8962083f4947faac8cf045da0487376c.mugqic.done
)
macs2_callpeak_71_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_71_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_72_JOB_ID: macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl2
JOB_DEPENDENCIES=$macs2_callpeak_71_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl2.b2cc14dbdd087010577d47c85acaf236.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl2.b2cc14dbdd087010577d47c85acaf236.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2_peaks.narrowPeak > peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_ckII_Ctl_Cl2/Iws1_ckII_Ctl_Cl2_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl2.b2cc14dbdd087010577d47c85acaf236.mugqic.done
)
macs2_callpeak_72_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_72_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_73_JOB_ID: macs2_callpeak.Iws1_ckII_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_ckII_Ctl_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_ckII_Ctl_Cl3.bc0b6678db92c5b716336f73b92a5677.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_ckII_Ctl_Cl3.bc0b6678db92c5b716336f73b92a5677.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_ckII_Ctl_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3 \
  >& peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3.diag.macs.out
macs2_callpeak.Iws1_ckII_Ctl_Cl3.bc0b6678db92c5b716336f73b92a5677.mugqic.done
)
macs2_callpeak_73_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_73_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_74_JOB_ID: macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_73_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl3.9acdb5671c53c5ac03ad7ba62a1d8e2e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl3.9acdb5671c53c5ac03ad7ba62a1d8e2e.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3_peaks.narrowPeak > peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_ckII_Ctl_Cl3/Iws1_ckII_Ctl_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_ckII_Ctl_Cl3.9acdb5671c53c5ac03ad7ba62a1d8e2e.mugqic.done
)
macs2_callpeak_74_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_74_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_75_JOB_ID: macs2_callpeak.Iws1_WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_WT_39C_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_WT_39C_Cl3.1b8d372ea7976e210b3933f66d909725.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_WT_39C_Cl3.1b8d372ea7976e210b3933f66d909725.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_WT_39C_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3 \
  >& peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3.diag.macs.out
macs2_callpeak.Iws1_WT_39C_Cl3.1b8d372ea7976e210b3933f66d909725.mugqic.done
)
macs2_callpeak_75_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_75_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_76_JOB_ID: macs2_callpeak_bigBed.Iws1_WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_WT_39C_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_75_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_WT_39C_Cl3.55bc5ef1daeaa6203676e2c5c0acd5b2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_WT_39C_Cl3.55bc5ef1daeaa6203676e2c5c0acd5b2.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3_peaks.narrowPeak > peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_WT_39C_Cl3/Iws1_WT_39C_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_WT_39C_Cl3.55bc5ef1daeaa6203676e2c5c0acd5b2.mugqic.done
)
macs2_callpeak_76_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_76_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_77_JOB_ID: macs2_callpeak.Iws1_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_spt6_39C_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_spt6_39C_Cl3.24f97c3b9527c98de325c8021b869329.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_spt6_39C_Cl3.24f97c3b9527c98de325c8021b869329.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_spt6_39C_Cl3 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_spt6-SA_39C_Cl3/Iws1_spt6-SA_39C_Cl3.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3 \
  >& peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3.diag.macs.out
macs2_callpeak.Iws1_spt6_39C_Cl3.24f97c3b9527c98de325c8021b869329.mugqic.done
)
macs2_callpeak_77_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_77_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_78_JOB_ID: macs2_callpeak_bigBed.Iws1_spt6_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_spt6_39C_Cl3
JOB_DEPENDENCIES=$macs2_callpeak_77_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_spt6_39C_Cl3.c3fad78d95ab82e0e21f6863114a9426.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_spt6_39C_Cl3.c3fad78d95ab82e0e21f6863114a9426.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3_peaks.narrowPeak > peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_spt6_39C_Cl3/Iws1_spt6_39C_Cl3_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_spt6_39C_Cl3.c3fad78d95ab82e0e21f6863114a9426.mugqic.done
)
macs2_callpeak_78_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_78_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_79_JOB_ID: macs2_callpeak.Iws1_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.Iws1_spt6_39C_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.Iws1_spt6_39C_Cl1.502977480183c111ff1a253f01a40fba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.Iws1_spt6_39C_Cl1.502977480183c111ff1a253f01a40fba.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/Iws1_spt6_39C_Cl1 && \
macs2 callpeak --format BAM --nomodel \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 9725684.0 \
  --treatment \
  alignment/Iws1_spt6-SA_39C_Cl1/Iws1_spt6-SA_39C_Cl1.sorted.dup.bam \
  --control \
  alignment/No-TAG/No-TAG.sorted.dup.bam \
  --name peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1 \
  >& peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1.diag.macs.out
macs2_callpeak.Iws1_spt6_39C_Cl1.502977480183c111ff1a253f01a40fba.mugqic.done
)
macs2_callpeak_79_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_79_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_80_JOB_ID: macs2_callpeak_bigBed.Iws1_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.Iws1_spt6_39C_Cl1
JOB_DEPENDENCIES=$macs2_callpeak_79_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.Iws1_spt6_39C_Cl1.00b76f3d9fbb0c9bbda6b6048fdd1f03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.Iws1_spt6_39C_Cl1.00b76f3d9fbb0c9bbda6b6048fdd1f03.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1_peaks.narrowPeak > peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1_peaks.narrowPeak.bed \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/Saccharomyces_cerevisiae.R64-1-1.fa.fai \
  peak_call/Iws1_spt6_39C_Cl1/Iws1_spt6_39C_Cl1_peaks.narrowPeak.bb
macs2_callpeak_bigBed.Iws1_spt6_39C_Cl1.00b76f3d9fbb0c9bbda6b6048fdd1f03.mugqic.done
)
macs2_callpeak_80_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_80_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_81_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_5_JOB_ID:$macs2_callpeak_7_JOB_ID:$macs2_callpeak_9_JOB_ID:$macs2_callpeak_11_JOB_ID:$macs2_callpeak_13_JOB_ID:$macs2_callpeak_15_JOB_ID:$macs2_callpeak_17_JOB_ID:$macs2_callpeak_19_JOB_ID:$macs2_callpeak_21_JOB_ID:$macs2_callpeak_23_JOB_ID:$macs2_callpeak_25_JOB_ID:$macs2_callpeak_27_JOB_ID:$macs2_callpeak_29_JOB_ID:$macs2_callpeak_31_JOB_ID:$macs2_callpeak_33_JOB_ID:$macs2_callpeak_35_JOB_ID:$macs2_callpeak_37_JOB_ID:$macs2_callpeak_39_JOB_ID:$macs2_callpeak_41_JOB_ID:$macs2_callpeak_43_JOB_ID:$macs2_callpeak_45_JOB_ID:$macs2_callpeak_47_JOB_ID:$macs2_callpeak_49_JOB_ID:$macs2_callpeak_51_JOB_ID:$macs2_callpeak_53_JOB_ID:$macs2_callpeak_55_JOB_ID:$macs2_callpeak_57_JOB_ID:$macs2_callpeak_59_JOB_ID:$macs2_callpeak_61_JOB_ID:$macs2_callpeak_63_JOB_ID:$macs2_callpeak_65_JOB_ID:$macs2_callpeak_67_JOB_ID:$macs2_callpeak_69_JOB_ID:$macs2_callpeak_71_JOB_ID:$macs2_callpeak_73_JOB_ID:$macs2_callpeak_75_JOB_ID:$macs2_callpeak_77_JOB_ID:$macs2_callpeak_79_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in Chd1-Myc_dspt2Cl2 Chd1-Myc_dspt2Cl1 Iws1-Myc_dspt2Cl2 Iws1-Myc_dspt2Cl1 Spt6-Myc_dspt2Cl2 Spt6-Myc_dspt2Cl1 Chd1-Myc_spt6_39C_Cl2 Chd1-Myc_spt6_39C_Cl1 Iws1-Myc_spt6_39C_Cl2 Iws1-Myc_spt6_39C_Cl1 Spt2-Myc_spt6_39C_Cl2 Spt2-Myc_spt6_39C_Cl1 Chd1-Myc_Wt_39C_Cl2 Chd1-Myc_Wt_39C_Cl1 Iws1-Myc_Wt_39C_Cl2 Iws1-Myc_Wt_39C_Cl1 Spt2-Myc_Wt_39C_Cl2 Spt2-Myc_Wt_39C_Cl1 Chd1-Myc_WtCl2 Chd1-Myc_WtCl1 Iws1-Myc_WtCl2 Iws1-Myc_WtCl1 Spt6-Myc_WtCl2 Spt6-Myc_WtCl1 Spt6_WT_39C_Cl3 Spt6_ckII_Ctl_Cl2 Spt6_spt6_39C_Cl3 Spt6_ckII_Ctl_Cl3 Spt6_WT_Ctl_Cl3 Spt6_WT_39C_Cl1 Spt6_spt6_39C_Cl1 Spt6_WT_Ctl_Cl1 Iws1_WT_Ctl_Cl1 Iws1_WT_Ctl_Cl3 Iws1_WT_39C_Cl1 Iws1_ckII_Ctl_Cl2 Iws1_ckII_Ctl_Cl3 Iws1_WT_39C_Cl3 Iws1_spt6_39C_Cl3 Iws1_spt6_39C_Cl1
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.cb9baa202a10232e0b41e3321b3adbef.mugqic.done
)
macs2_callpeak_81_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_81_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
LOG_MD5=$(echo $USER-'206.12.124.6-ChipSeq-Iws1-Myc_dspt2Cl2.Iws1-Myc_dspt2Cl2_RS,Iws1-Myc_dspt2Cl1.Iws1-Myc_dspt2Cl1_RS,Iws1_spt6-SA_39C_Cl1.Iws1_spt6-SA_39C_Cl1_RS,Chd1-Myc_WtCl1.Chd1-Myc_WtCl1_RS,Chd1-Myc_WtCl2.Chd1-Myc_WtCl2_RS,Spt2-Myc_spt6_39C_Cl2.Spt2-Myc_spt6_39C_Cl2_RS,Spt6-Myc_dspt2Cl2.Spt6-Myc_dspt2Cl2_RS,Spt6-Myc_dspt2Cl1.Spt6-Myc_dspt2Cl1_RS,Spt2-Myc_spt6_39C_Cl1.Spt2-Myc_spt6_39C_Cl1_RS,Iws1_WT_Ctl_Cl1.Iws1_WT_Ctl_Cl1_RS,Iws1-Myc_WtCl1.Iws1-Myc_WtCl1_RS,Spt6_WT_39C_Cl1.Spt6_WT_39C_Cl1_RS,Iws1-Myc_WtCl2.Iws1-Myc_WtCl2_RS,Iws1_ckII_Ctl_Cl2.Iws1_ckII_Ctl_Cl2_RS,Spt2-Myc_Wt_39C_Cl2.Spt2-Myc_Wt_39C_Cl2_RS,Spt6_WT_Ctl_Cl1.Spt6_WT_Ctl_Cl1_RS,Iws1_spt6-SA_39C_Cl3.Iws1_spt6-SA_39C_Cl3_RS,Spt2-Myc_Wt_39C_Cl1.Spt2-Myc_Wt_39C_Cl1_RS,No-TAG.No-TAG_RS,Iws1_WT_39C_Cl1.Iws1_WT_39C_Cl1_RS,Iws1_WT_39C_Cl3.Iws1_WT_39C_Cl3_RS,Chd1-Myc_spt6_39C_Cl2.Chd1-Myc_spt6_39C_Cl2_RS,Chd1-Myc_spt6_39C_Cl1.Chd1-Myc_spt6_39C_Cl1_RS,Iws1_ckII_Ctl_Cl3.Iws1_ckII_Ctl_Cl3_RS,Iws1-Myc_spt6_39C_Cl2.Iws1-Myc_spt6_39C_Cl2_RS,Iws1-Myc_spt6_39C_Cl1.Iws1-Myc_spt6_39C_Cl1_RS,Spt6-Myc_WtCl2.Spt6-Myc_WtCl2_RS,Spt6_ckII_Ctl_Cl2.Spt6_ckII_Ctl_Cl2_RS,Spt6-Myc_WtCl1.Spt6-Myc_WtCl1_RS,Chd1-Myc_Wt_39C_Cl2.Chd1-Myc_Wt_39C_Cl2_RS,Chd1-Myc_Wt_39C_Cl1.Chd1-Myc_Wt_39C_Cl1_RS,Chd1-Myc_dspt2Cl2.Chd1-Myc_dspt2Cl2_RS,Spt6_ckII_Ctl_Cl3.Spt6_ckII_Ctl_Cl3_RS,Iws1_WT_Ctl_Cl3.Iws1_WT_Ctl_Cl3_RS,Chd1-Myc_dspt2Cl1.Chd1-Myc_dspt2Cl1_RS,Spt6_spt6-SA_39C_Cl3.Spt6_spt6-SA_39C_Cl3_RS,Spt6_spt6-SA_39C_Cl1.Spt6_spt6-SA_39C_Cl1_RS,Spt6_WT_39C_Cl3.Spt6_WT_39C_Cl3_RS,Spt6_WT_Ctl_Cl3.Spt6_WT_Ctl_Cl3_RS,Iws1-Myc_Wt_39C_Cl2.Iws1-Myc_Wt_39C_Cl2_RS,Iws1-Myc_Wt_39C_Cl1.Iws1-Myc_Wt_39C_Cl1_RS' | md5sum | awk '{ print $1 }')
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,macs2_callpeak&samples=41&md5=$LOG_MD5" --quiet --output-document=/dev/null

