#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-10-03T19:39:32
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 24 jobs
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 26 jobs
#   samtools_view_filter: 26 jobs
#   picard_merge_sam_files: 25 jobs
#   TOTAL: 102 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6007406/efournie/Chaperone/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.Chd1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_dspt2Cl2_RS.58f8d7354ec55fd7b02c5c6c9252541a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_dspt2Cl2_RS.58f8d7354ec55fd7b02c5c6c9252541a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_dspt2Cl2 && \
`cat > trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_8.Chd1-Myc_dspt2Cl2_R1.fastq.gz \
  trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.log
trimmomatic.Chd1-Myc_dspt2Cl2_RS.58f8d7354ec55fd7b02c5c6c9252541a.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.Chd1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_dspt2Cl1_RS.64f4cab76ace92e20010a525f02128d0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_dspt2Cl1_RS.64f4cab76ace92e20010a525f02128d0.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_dspt2Cl1 && \
`cat > trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_9.Chd1-Myc_dspt2Cl1_R1.fastq.gz \
  trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.log
trimmomatic.Chd1-Myc_dspt2Cl1_RS.64f4cab76ace92e20010a525f02128d0.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.Iws1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_dspt2Cl2_RS.786a12aa8a2d852ad6d4b31a8edae068.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_dspt2Cl2_RS.786a12aa8a2d852ad6d4b31a8edae068.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_dspt2Cl2 && \
`cat > trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_15.Iws1-Myc_dspt2Cl2_R1.fastq.gz \
  trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.log
trimmomatic.Iws1-Myc_dspt2Cl2_RS.786a12aa8a2d852ad6d4b31a8edae068.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.Iws1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_dspt2Cl1_RS.8c0829fef2f757ebd2460197915d70f4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_dspt2Cl1_RS.8c0829fef2f757ebd2460197915d70f4.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_dspt2Cl1 && \
`cat > trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_6.Iws1-Myc_dspt2Cl1_R1.fastq.gz \
  trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.log
trimmomatic.Iws1-Myc_dspt2Cl1_RS.8c0829fef2f757ebd2460197915d70f4.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.Spt6-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt6-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt6-Myc_dspt2Cl2_RS.06288b307c9a7757b98f721f1701e190.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt6-Myc_dspt2Cl2_RS.06288b307c9a7757b98f721f1701e190.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt6-Myc_dspt2Cl2 && \
`cat > trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_27.Spt6-Myc_dspt2Cl2_R1.fastq.gz \
  trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.log
trimmomatic.Spt6-Myc_dspt2Cl2_RS.06288b307c9a7757b98f721f1701e190.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.Spt6-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt6-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt6-Myc_dspt2Cl1_RS.9c27af1359faddc93b6d1c2f5ffa6235.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt6-Myc_dspt2Cl1_RS.9c27af1359faddc93b6d1c2f5ffa6235.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt6-Myc_dspt2Cl1 && \
`cat > trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_23.Spt6-Myc_dspt2Cl1_R1.fastq.gz \
  trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.log
trimmomatic.Spt6-Myc_dspt2Cl1_RS.9c27af1359faddc93b6d1c2f5ffa6235.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_7_JOB_ID: trimmomatic.Chd1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_spt6_39C_Cl2_RS.274d91f803db770ac2a4efc43aa8520b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_spt6_39C_Cl2_RS.274d91f803db770ac2a4efc43aa8520b.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_spt6_39C_Cl2 && \
`cat > trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_22.Chd1-Myc_spt6_39C_Cl2_R1.fastq.gz \
  trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.log
trimmomatic.Chd1-Myc_spt6_39C_Cl2_RS.274d91f803db770ac2a4efc43aa8520b.mugqic.done
)
trimmomatic_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_8_JOB_ID: trimmomatic.Chd1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_spt6_39C_Cl1_RS.513c6b1c781054442d4fe6815eedd7fc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_spt6_39C_Cl1_RS.513c6b1c781054442d4fe6815eedd7fc.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_spt6_39C_Cl1 && \
`cat > trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_20.Chd1-Myc_spt6_39C_Cl1_R1.fastq.gz \
  trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.log
trimmomatic.Chd1-Myc_spt6_39C_Cl1_RS.513c6b1c781054442d4fe6815eedd7fc.mugqic.done
)
trimmomatic_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_9_JOB_ID: trimmomatic.Iws1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_spt6_39C_Cl2_RS.3eb409dc2d3eb0bf738588e46633fa96.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_spt6_39C_Cl2_RS.3eb409dc2d3eb0bf738588e46633fa96.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_spt6_39C_Cl2 && \
`cat > trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_16.Iws1-Myc_spt6_39C_Cl2_R1.fastq.gz \
  trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.log
trimmomatic.Iws1-Myc_spt6_39C_Cl2_RS.3eb409dc2d3eb0bf738588e46633fa96.mugqic.done
)
trimmomatic_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_10_JOB_ID: trimmomatic.Iws1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_spt6_39C_Cl1_RS.dc45d2790331f3ee13ef05a993a97eb7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_spt6_39C_Cl1_RS.dc45d2790331f3ee13ef05a993a97eb7.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_spt6_39C_Cl1 && \
`cat > trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_14.Iws1-Myc_spt6_39C_Cl1_R1.fastq.gz \
  trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.log
trimmomatic.Iws1-Myc_spt6_39C_Cl1_RS.dc45d2790331f3ee13ef05a993a97eb7.mugqic.done
)
trimmomatic_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_11_JOB_ID: trimmomatic.Spt2-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt2-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt2-Myc_spt6_39C_Cl2_RS.45b2655b7ab05b5c5102a2cbc069050d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt2-Myc_spt6_39C_Cl2_RS.45b2655b7ab05b5c5102a2cbc069050d.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt2-Myc_spt6_39C_Cl2 && \
`cat > trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_19.Spt2-Myc_spt6_39C_Cl2_R1.fastq.gz \
  trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.log
trimmomatic.Spt2-Myc_spt6_39C_Cl2_RS.45b2655b7ab05b5c5102a2cbc069050d.mugqic.done
)
trimmomatic_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_12_JOB_ID: trimmomatic.Spt2-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt2-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt2-Myc_spt6_39C_Cl1_RS.38818fe13486b6b959bc8782b05ed725.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt2-Myc_spt6_39C_Cl1_RS.38818fe13486b6b959bc8782b05ed725.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt2-Myc_spt6_39C_Cl1 && \
`cat > trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_12.Spt2-Myc_spt6_39C_Cl1_R1.fastq.gz \
  trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.log
trimmomatic.Spt2-Myc_spt6_39C_Cl1_RS.38818fe13486b6b959bc8782b05ed725.mugqic.done
)
trimmomatic_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_13_JOB_ID: trimmomatic.Chd1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_Wt_39C_Cl2_RS.852254eaa64fc9c0372bb9a681246726.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_Wt_39C_Cl2_RS.852254eaa64fc9c0372bb9a681246726.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_Wt_39C_Cl2 && \
`cat > trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_11.Chd1-Myc_Wt_39C_Cl2_R1.fastq.gz \
  trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.log
trimmomatic.Chd1-Myc_Wt_39C_Cl2_RS.852254eaa64fc9c0372bb9a681246726.mugqic.done
)
trimmomatic_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_14_JOB_ID: trimmomatic.Chd1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_Wt_39C_Cl1_RS.2236d8717793d93682900fac85b8452b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_Wt_39C_Cl1_RS.2236d8717793d93682900fac85b8452b.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_Wt_39C_Cl1 && \
`cat > trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_10.Chd1-Myc_Wt_39C_Cl1_R1.fastq.gz \
  trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.log
trimmomatic.Chd1-Myc_Wt_39C_Cl1_RS.2236d8717793d93682900fac85b8452b.mugqic.done
)
trimmomatic_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_15_JOB_ID: trimmomatic.Iws1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_Wt_39C_Cl2_RS.c01db1eff99b1dbe57302697179c015a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_Wt_39C_Cl2_RS.c01db1eff99b1dbe57302697179c015a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_Wt_39C_Cl2 && \
`cat > trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_18.Iws1-Myc_Wt_39C_Cl2_R1.fastq.gz \
  trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.log
trimmomatic.Iws1-Myc_Wt_39C_Cl2_RS.c01db1eff99b1dbe57302697179c015a.mugqic.done
)
trimmomatic_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_16_JOB_ID: trimmomatic.Iws1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_Wt_39C_Cl1_RS.2d811a519aa15a3cac0743677593350a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_Wt_39C_Cl1_RS.2d811a519aa15a3cac0743677593350a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_Wt_39C_Cl1 && \
`cat > trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_7.Iws1-Myc_Wt_39C_Cl1_R1.fastq.gz \
  trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.log
trimmomatic.Iws1-Myc_Wt_39C_Cl1_RS.2d811a519aa15a3cac0743677593350a.mugqic.done
)
trimmomatic_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_17_JOB_ID: trimmomatic.Spt2-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt2-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt2-Myc_Wt_39C_Cl1_RS.e1ef26d6b5264df2f811725532606be8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt2-Myc_Wt_39C_Cl1_RS.e1ef26d6b5264df2f811725532606be8.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt2-Myc_Wt_39C_Cl1 && \
`cat > trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_4.Spt2-Myc_Wt_39C_Cl1_R1.fastq.gz \
  trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.log
trimmomatic.Spt2-Myc_Wt_39C_Cl1_RS.e1ef26d6b5264df2f811725532606be8.mugqic.done
)
trimmomatic_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_18_JOB_ID: trimmomatic.Chd1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_WtCl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_WtCl2_RS.af877df6a21d1dedf005d0618d43e3a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_WtCl2_RS.af877df6a21d1dedf005d0618d43e3a8.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_WtCl2 && \
`cat > trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_3.Chd1-Myc_WtCl2_R1.fastq.gz \
  trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.log
trimmomatic.Chd1-Myc_WtCl2_RS.af877df6a21d1dedf005d0618d43e3a8.mugqic.done
)
trimmomatic_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_19_JOB_ID: trimmomatic.Chd1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Chd1-Myc_WtCl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Chd1-Myc_WtCl1_RS.edb238062209b5300b9d5bb879b8381b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Chd1-Myc_WtCl1_RS.edb238062209b5300b9d5bb879b8381b.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Chd1-Myc_WtCl1 && \
`cat > trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_1.Chd1-Myc_WtCl1_R1.fastq.gz \
  trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.log
trimmomatic.Chd1-Myc_WtCl1_RS.edb238062209b5300b9d5bb879b8381b.mugqic.done
)
trimmomatic_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_20_JOB_ID: trimmomatic.Iws1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_WtCl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_WtCl2_RS.788a462b0cc1dece13a18cbd2c5be85a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_WtCl2_RS.788a462b0cc1dece13a18cbd2c5be85a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_WtCl2 && \
`cat > trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_13.Iws1-Myc_WtCl2_R1.fastq.gz \
  trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.log
trimmomatic.Iws1-Myc_WtCl2_RS.788a462b0cc1dece13a18cbd2c5be85a.mugqic.done
)
trimmomatic_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_21_JOB_ID: trimmomatic.Iws1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Iws1-Myc_WtCl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Iws1-Myc_WtCl1_RS.8b41d2c6887ed4c3f6f647257f659f15.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Iws1-Myc_WtCl1_RS.8b41d2c6887ed4c3f6f647257f659f15.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Iws1-Myc_WtCl1 && \
`cat > trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4097.008.Index_2.Iws1-Myc_WtCl1_R1.fastq.gz \
  trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.log
trimmomatic.Iws1-Myc_WtCl1_RS.8b41d2c6887ed4c3f6f647257f659f15.mugqic.done
)
trimmomatic_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_22_JOB_ID: trimmomatic.Spt6-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt6-Myc_WtCl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt6-Myc_WtCl2_RS.ef5cc7016c79a0d0ae421b76aff0be85.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt6-Myc_WtCl2_RS.ef5cc7016c79a0d0ae421b76aff0be85.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt6-Myc_WtCl2 && \
`cat > trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_21.Spt6-Myc_WtCl2_R1.fastq.gz \
  trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.log
trimmomatic.Spt6-Myc_WtCl2_RS.ef5cc7016c79a0d0ae421b76aff0be85.mugqic.done
)
trimmomatic_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_23_JOB_ID: trimmomatic.Spt6-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.Spt6-Myc_WtCl1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.Spt6-Myc_WtCl1_RS.acd00249efb7e19d577ade9938d2db09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.Spt6-Myc_WtCl1_RS.acd00249efb7e19d577ade9938d2db09.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Spt6-Myc_WtCl1 && \
`cat > trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_25.Spt6-Myc_WtCl1_R1.fastq.gz \
  trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.log
trimmomatic.Spt6-Myc_WtCl1_RS.acd00249efb7e19d577ade9938d2db09.mugqic.done
)
trimmomatic_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_24_JOB_ID: trimmomatic.No-TAG_RS
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.No-TAG_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.No-TAG_RS.7c6dadeb0a28f04dc9177b76ebd7b8dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.No-TAG_RS.7c6dadeb0a28f04dc9177b76ebd7b8dc.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/No-TAG && \
`cat > trim/No-TAG/No-TAG_RS.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Chaperone/raw/HI.4117.006.Index_2.No-TAG_R1.fastq.gz \
  trim/No-TAG/No-TAG_RS.trim.single.fastq.gz \
  ILLUMINACLIP:trim/No-TAG/No-TAG_RS.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/No-TAG/No-TAG_RS.trim.log
trimmomatic.No-TAG_RS.7c6dadeb0a28f04dc9177b76ebd7b8dc.mugqic.done
)
trimmomatic_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID:$trimmomatic_7_JOB_ID:$trimmomatic_8_JOB_ID:$trimmomatic_9_JOB_ID:$trimmomatic_10_JOB_ID:$trimmomatic_11_JOB_ID:$trimmomatic_12_JOB_ID:$trimmomatic_13_JOB_ID:$trimmomatic_14_JOB_ID:$trimmomatic_15_JOB_ID:$trimmomatic_16_JOB_ID:$trimmomatic_17_JOB_ID:$trimmomatic_18_JOB_ID:$trimmomatic_19_JOB_ID:$trimmomatic_20_JOB_ID:$trimmomatic_21_JOB_ID:$trimmomatic_22_JOB_ID:$trimmomatic_23_JOB_ID:$trimmomatic_24_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.e21d5cd2bd30ff2a91d4997c5a33104a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.e21d5cd2bd30ff2a91d4997c5a33104a.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_dspt2Cl2	Chd1-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_dspt2Cl1	Chd1-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_dspt2Cl2	Iws1-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_dspt2Cl1	Iws1-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_dspt2Cl2	Spt6-Myc_dspt2Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_dspt2Cl1	Spt6-Myc_dspt2Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_spt6_39C_Cl2	Chd1-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_spt6_39C_Cl1	Chd1-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_spt6_39C_Cl2	Iws1-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_spt6_39C_Cl1	Iws1-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_spt6_39C_Cl2	Spt2-Myc_spt6_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_spt6_39C_Cl1	Spt2-Myc_spt6_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_Wt_39C_Cl2	Chd1-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_Wt_39C_Cl1	Chd1-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_Wt_39C_Cl2	Iws1-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_Wt_39C_Cl1	Iws1-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_Wt_39C_Cl2	Spt2-Myc_Wt_39C_Cl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt2-Myc_Wt_39C_Cl1	Spt2-Myc_Wt_39C_Cl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_WtCl2	Chd1-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Chd1-Myc_WtCl1	Chd1-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_WtCl2	Iws1-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Iws1-Myc_WtCl1	Iws1-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_WtCl2	Spt6-Myc_WtCl2_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Spt6-Myc_WtCl1	Spt6-Myc_WtCl1_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/No-TAG/No-TAG_RS.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/No-TAG	No-TAG_RS	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.e21d5cd2bd30ff2a91d4997c5a33104a.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json,/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json,/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl2_RS.876066c8bf0b1a8ae81035a596945add.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl2_RS.876066c8bf0b1a8ae81035a596945add.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_dspt2Cl2_RS	SM:Chd1-Myc_dspt2Cl2	LB:Chd1-Myc_dspt2Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS/Chd1-Myc_dspt2Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl2_RS.876066c8bf0b1a8ae81035a596945add.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl1_RS.c99810566f31c4c8d8b7b4f06b528ebb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl1_RS.c99810566f31c4c8d8b7b4f06b528ebb.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_dspt2Cl1_RS	SM:Chd1-Myc_dspt2Cl1	LB:Chd1-Myc_dspt2Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS/Chd1-Myc_dspt2Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_dspt2Cl1_RS.c99810566f31c4c8d8b7b4f06b528ebb.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_3_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl2_RS.8fff220d713206ce15f8a6d5b649396b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl2_RS.8fff220d713206ce15f8a6d5b649396b.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_dspt2Cl2_RS	SM:Iws1-Myc_dspt2Cl2	LB:Iws1-Myc_dspt2Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS/Iws1-Myc_dspt2Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl2_RS.8fff220d713206ce15f8a6d5b649396b.mugqic.done
)
bwa_mem_picard_sort_sam_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_4_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl1_RS.6c514df35116bd100a2241b7ac702670.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl1_RS.6c514df35116bd100a2241b7ac702670.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_dspt2Cl1_RS	SM:Iws1-Myc_dspt2Cl1	LB:Iws1-Myc_dspt2Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS/Iws1-Myc_dspt2Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_dspt2Cl1_RS.6c514df35116bd100a2241b7ac702670.mugqic.done
)
bwa_mem_picard_sort_sam_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_5_JOB_ID: bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl2_RS.9cb4a38d0d9caa27d79dcfe42a008061.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl2_RS.9cb4a38d0d9caa27d79dcfe42a008061.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt6-Myc_dspt2Cl2_RS	SM:Spt6-Myc_dspt2Cl2	LB:Spt6-Myc_dspt2Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS/Spt6-Myc_dspt2Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl2_RS.9cb4a38d0d9caa27d79dcfe42a008061.mugqic.done
)
bwa_mem_picard_sort_sam_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_6_JOB_ID: bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl1_RS.dfb532704d50ebd08726c7ffd28ec985.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl1_RS.dfb532704d50ebd08726c7ffd28ec985.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt6-Myc_dspt2Cl1_RS	SM:Spt6-Myc_dspt2Cl1	LB:Spt6-Myc_dspt2Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS/Spt6-Myc_dspt2Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt6-Myc_dspt2Cl1_RS.dfb532704d50ebd08726c7ffd28ec985.mugqic.done
)
bwa_mem_picard_sort_sam_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_7_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl2_RS.c6f0e0ad9565ce9da66520caed8744c7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl2_RS.c6f0e0ad9565ce9da66520caed8744c7.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_spt6_39C_Cl2_RS	SM:Chd1-Myc_spt6_39C_Cl2	LB:Chd1-Myc_spt6_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS/Chd1-Myc_spt6_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl2_RS.c6f0e0ad9565ce9da66520caed8744c7.mugqic.done
)
bwa_mem_picard_sort_sam_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_8_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl1_RS.603b524e8e9277b2f43dfe9a93eccd95.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl1_RS.603b524e8e9277b2f43dfe9a93eccd95.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_spt6_39C_Cl1_RS	SM:Chd1-Myc_spt6_39C_Cl1	LB:Chd1-Myc_spt6_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS/Chd1-Myc_spt6_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_spt6_39C_Cl1_RS.603b524e8e9277b2f43dfe9a93eccd95.mugqic.done
)
bwa_mem_picard_sort_sam_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_9_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl2_RS.aa9e7f0adab96976e97bb3e4eb2d095a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl2_RS.aa9e7f0adab96976e97bb3e4eb2d095a.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_spt6_39C_Cl2_RS	SM:Iws1-Myc_spt6_39C_Cl2	LB:Iws1-Myc_spt6_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS/Iws1-Myc_spt6_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl2_RS.aa9e7f0adab96976e97bb3e4eb2d095a.mugqic.done
)
bwa_mem_picard_sort_sam_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_10_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_10_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl1_RS.b82c8f5425cc869a80e7346ae709c785.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl1_RS.b82c8f5425cc869a80e7346ae709c785.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_spt6_39C_Cl1_RS	SM:Iws1-Myc_spt6_39C_Cl1	LB:Iws1-Myc_spt6_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS/Iws1-Myc_spt6_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_spt6_39C_Cl1_RS.b82c8f5425cc869a80e7346ae709c785.mugqic.done
)
bwa_mem_picard_sort_sam_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_11_JOB_ID: bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_11_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl2_RS.a837372caa1ef203e76206391a66f516.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl2_RS.a837372caa1ef203e76206391a66f516.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt2-Myc_spt6_39C_Cl2_RS	SM:Spt2-Myc_spt6_39C_Cl2	LB:Spt2-Myc_spt6_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS/Spt2-Myc_spt6_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl2_RS.a837372caa1ef203e76206391a66f516.mugqic.done
)
bwa_mem_picard_sort_sam_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_12_JOB_ID: bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_12_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl1_RS.47236a18876f60693d1836e6c045cf60.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl1_RS.47236a18876f60693d1836e6c045cf60.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt2-Myc_spt6_39C_Cl1_RS	SM:Spt2-Myc_spt6_39C_Cl1	LB:Spt2-Myc_spt6_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS/Spt2-Myc_spt6_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt2-Myc_spt6_39C_Cl1_RS.47236a18876f60693d1836e6c045cf60.mugqic.done
)
bwa_mem_picard_sort_sam_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_13_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_13_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl2_RS.40364a225d760551f7a8e692b91751d6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl2_RS.40364a225d760551f7a8e692b91751d6.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_Wt_39C_Cl2_RS	SM:Chd1-Myc_Wt_39C_Cl2	LB:Chd1-Myc_Wt_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS/Chd1-Myc_Wt_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl2_RS.40364a225d760551f7a8e692b91751d6.mugqic.done
)
bwa_mem_picard_sort_sam_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_14_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_14_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl1_RS.b185b35774d01ce5ba66b0e6cf857b26.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl1_RS.b185b35774d01ce5ba66b0e6cf857b26.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_Wt_39C_Cl1_RS	SM:Chd1-Myc_Wt_39C_Cl1	LB:Chd1-Myc_Wt_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS/Chd1-Myc_Wt_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_Wt_39C_Cl1_RS.b185b35774d01ce5ba66b0e6cf857b26.mugqic.done
)
bwa_mem_picard_sort_sam_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_15_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=$trimmomatic_15_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl2_RS.9b6eb900476d4e59891639db2c5d2474.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl2_RS.9b6eb900476d4e59891639db2c5d2474.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_Wt_39C_Cl2_RS	SM:Iws1-Myc_Wt_39C_Cl2	LB:Iws1-Myc_Wt_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS/Iws1-Myc_Wt_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl2_RS.9b6eb900476d4e59891639db2c5d2474.mugqic.done
)
bwa_mem_picard_sort_sam_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_16_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_16_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl1_RS.5948be2f5a56f69636d5c6fdb7a663ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl1_RS.5948be2f5a56f69636d5c6fdb7a663ed.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_Wt_39C_Cl1_RS	SM:Iws1-Myc_Wt_39C_Cl1	LB:Iws1-Myc_Wt_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS/Iws1-Myc_Wt_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_Wt_39C_Cl1_RS.5948be2f5a56f69636d5c6fdb7a663ed.mugqic.done
)
bwa_mem_picard_sort_sam_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_17_JOB_ID: bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl2_RS.d8e33ca23c0e9f16f7d9ea00fc307f9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl2_RS.d8e33ca23c0e9f16f7d9ea00fc307f9c.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt2-Myc_Wt_39C_Cl2_RS	SM:Spt2-Myc_Wt_39C_Cl2	LB:Spt2-Myc_Wt_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS/Spt2-Myc_Wt_39C_Cl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl2_RS.d8e33ca23c0e9f16f7d9ea00fc307f9c.mugqic.done
)
bwa_mem_picard_sort_sam_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_18_JOB_ID: bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$trimmomatic_17_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl1_RS.671f0597b3237765e0c80a3b4149dc49.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl1_RS.671f0597b3237765e0c80a3b4149dc49.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt2-Myc_Wt_39C_Cl1_RS	SM:Spt2-Myc_Wt_39C_Cl1	LB:Spt2-Myc_Wt_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS/Spt2-Myc_Wt_39C_Cl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt2-Myc_Wt_39C_Cl1_RS.671f0597b3237765e0c80a3b4149dc49.mugqic.done
)
bwa_mem_picard_sort_sam_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_19_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_WtCl2_RS
JOB_DEPENDENCIES=$trimmomatic_18_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_WtCl2_RS.a751c7de4f0848467f3bc5a4aaf684e5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_WtCl2_RS.a751c7de4f0848467f3bc5a4aaf684e5.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_WtCl2_RS	SM:Chd1-Myc_WtCl2	LB:Chd1-Myc_WtCl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS/Chd1-Myc_WtCl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_WtCl2_RS.a751c7de4f0848467f3bc5a4aaf684e5.mugqic.done
)
bwa_mem_picard_sort_sam_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_20_JOB_ID: bwa_mem_picard_sort_sam.Chd1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Chd1-Myc_WtCl1_RS
JOB_DEPENDENCIES=$trimmomatic_19_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Chd1-Myc_WtCl1_RS.8a3e03197b5d9dffd3056ec4f32cf18f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Chd1-Myc_WtCl1_RS.8a3e03197b5d9dffd3056ec4f32cf18f.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Chd1-Myc_WtCl1_RS	SM:Chd1-Myc_WtCl1	LB:Chd1-Myc_WtCl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS/Chd1-Myc_WtCl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Chd1-Myc_WtCl1_RS.8a3e03197b5d9dffd3056ec4f32cf18f.mugqic.done
)
bwa_mem_picard_sort_sam_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_21_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_WtCl2_RS
JOB_DEPENDENCIES=$trimmomatic_20_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_WtCl2_RS.6bf3ec8443b48438cf6e5772187ec619.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_WtCl2_RS.6bf3ec8443b48438cf6e5772187ec619.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_WtCl2_RS	SM:Iws1-Myc_WtCl2	LB:Iws1-Myc_WtCl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS/Iws1-Myc_WtCl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_WtCl2_RS.6bf3ec8443b48438cf6e5772187ec619.mugqic.done
)
bwa_mem_picard_sort_sam_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_22_JOB_ID: bwa_mem_picard_sort_sam.Iws1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Iws1-Myc_WtCl1_RS
JOB_DEPENDENCIES=$trimmomatic_21_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Iws1-Myc_WtCl1_RS.6bd683f4c3380a81dba9ac5628b512c1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Iws1-Myc_WtCl1_RS.6bd683f4c3380a81dba9ac5628b512c1.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Iws1-Myc_WtCl1_RS	SM:Iws1-Myc_WtCl1	LB:Iws1-Myc_WtCl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS/Iws1-Myc_WtCl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Iws1-Myc_WtCl1_RS.6bd683f4c3380a81dba9ac5628b512c1.mugqic.done
)
bwa_mem_picard_sort_sam_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_23_JOB_ID: bwa_mem_picard_sort_sam.Spt6-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt6-Myc_WtCl2_RS
JOB_DEPENDENCIES=$trimmomatic_22_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt6-Myc_WtCl2_RS.032ebb63fac89d983973d6550390fa80.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt6-Myc_WtCl2_RS.032ebb63fac89d983973d6550390fa80.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt6-Myc_WtCl2_RS	SM:Spt6-Myc_WtCl2	LB:Spt6-Myc_WtCl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS/Spt6-Myc_WtCl2_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt6-Myc_WtCl2_RS.032ebb63fac89d983973d6550390fa80.mugqic.done
)
bwa_mem_picard_sort_sam_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_24_JOB_ID: bwa_mem_picard_sort_sam.Spt6-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.Spt6-Myc_WtCl1_RS
JOB_DEPENDENCIES=$trimmomatic_23_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.Spt6-Myc_WtCl1_RS.f8597c42a4b16a183f76e0018eec47a4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.Spt6-Myc_WtCl1_RS.f8597c42a4b16a183f76e0018eec47a4.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:Spt6-Myc_WtCl1_RS	SM:Spt6-Myc_WtCl1	LB:Spt6-Myc_WtCl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS/Spt6-Myc_WtCl1_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.Spt6-Myc_WtCl1_RS.f8597c42a4b16a183f76e0018eec47a4.mugqic.done
)
bwa_mem_picard_sort_sam_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_25_JOB_ID: bwa_mem_picard_sort_sam.No-TAG_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.No-TAG_RS
JOB_DEPENDENCIES=$trimmomatic_24_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.No-TAG_RS.3fa8146e6d6ff4da9cbd41115dccd6bb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.No-TAG_RS.3fa8146e6d6ff4da9cbd41115dccd6bb.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/No-TAG/No-TAG_RS && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:No-TAG_RS	SM:No-TAG	LB:No-TAG	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/No-TAG/No-TAG_RS.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/No-TAG/No-TAG_RS/No-TAG_RS.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.No-TAG_RS.3fa8146e6d6ff4da9cbd41115dccd6bb.mugqic.done
)
bwa_mem_picard_sort_sam_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_26_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID:$bwa_mem_picard_sort_sam_2_JOB_ID:$bwa_mem_picard_sort_sam_3_JOB_ID:$bwa_mem_picard_sort_sam_4_JOB_ID:$bwa_mem_picard_sort_sam_5_JOB_ID:$bwa_mem_picard_sort_sam_6_JOB_ID:$bwa_mem_picard_sort_sam_7_JOB_ID:$bwa_mem_picard_sort_sam_8_JOB_ID:$bwa_mem_picard_sort_sam_9_JOB_ID:$bwa_mem_picard_sort_sam_10_JOB_ID:$bwa_mem_picard_sort_sam_11_JOB_ID:$bwa_mem_picard_sort_sam_12_JOB_ID:$bwa_mem_picard_sort_sam_13_JOB_ID:$bwa_mem_picard_sort_sam_14_JOB_ID:$bwa_mem_picard_sort_sam_15_JOB_ID:$bwa_mem_picard_sort_sam_16_JOB_ID:$bwa_mem_picard_sort_sam_17_JOB_ID:$bwa_mem_picard_sort_sam_18_JOB_ID:$bwa_mem_picard_sort_sam_19_JOB_ID:$bwa_mem_picard_sort_sam_20_JOB_ID:$bwa_mem_picard_sort_sam_21_JOB_ID:$bwa_mem_picard_sort_sam_22_JOB_ID:$bwa_mem_picard_sort_sam_23_JOB_ID:$bwa_mem_picard_sort_sam_24_JOB_ID:$bwa_mem_picard_sort_sam_25_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.bf2abf224a616152e67c7d5334f65573.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.bf2abf224a616152e67c7d5334f65573.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Saccharomyces_cerevisiae" \
  --variable assembly="R64-1-1" \
  /home/efournie/genpipes/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.bf2abf224a616152e67c7d5334f65573.mugqic.done
)
bwa_mem_picard_sort_sam_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.Chd1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_dspt2Cl2_RS.f70bd0714bce22e27f509d43f16be8d9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_dspt2Cl2_RS.f70bd0714bce22e27f509d43f16be8d9.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS/Chd1-Myc_dspt2Cl2_RS.sorted.bam \
  > alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2_RS/Chd1-Myc_dspt2Cl2_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_dspt2Cl2_RS.f70bd0714bce22e27f509d43f16be8d9.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.Chd1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_dspt2Cl1_RS.8b6da95ee637d61f0d5e7e95ee8c3839.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_dspt2Cl1_RS.8b6da95ee637d61f0d5e7e95ee8c3839.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS/Chd1-Myc_dspt2Cl1_RS.sorted.bam \
  > alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1_RS/Chd1-Myc_dspt2Cl1_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_dspt2Cl1_RS.8b6da95ee637d61f0d5e7e95ee8c3839.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.Iws1-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_dspt2Cl2_RS.78654ad15ca271cfb4f087f0e7161bdf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_dspt2Cl2_RS.78654ad15ca271cfb4f087f0e7161bdf.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS/Iws1-Myc_dspt2Cl2_RS.sorted.bam \
  > alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2_RS/Iws1-Myc_dspt2Cl2_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_dspt2Cl2_RS.78654ad15ca271cfb4f087f0e7161bdf.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter.Iws1-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_dspt2Cl1_RS.c02195b393c6b9cd1a93828cef68b4c7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_dspt2Cl1_RS.c02195b393c6b9cd1a93828cef68b4c7.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS/Iws1-Myc_dspt2Cl1_RS.sorted.bam \
  > alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1_RS/Iws1-Myc_dspt2Cl1_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_dspt2Cl1_RS.c02195b393c6b9cd1a93828cef68b4c7.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_5_JOB_ID: samtools_view_filter.Spt6-Myc_dspt2Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt6-Myc_dspt2Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt6-Myc_dspt2Cl2_RS.711d2d8e174b0306fc32ac1e95a8e088.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt6-Myc_dspt2Cl2_RS.711d2d8e174b0306fc32ac1e95a8e088.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS/Spt6-Myc_dspt2Cl2_RS.sorted.bam \
  > alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2_RS/Spt6-Myc_dspt2Cl2_RS.sorted.filtered.bam
samtools_view_filter.Spt6-Myc_dspt2Cl2_RS.711d2d8e174b0306fc32ac1e95a8e088.mugqic.done
)
samtools_view_filter_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_6_JOB_ID: samtools_view_filter.Spt6-Myc_dspt2Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt6-Myc_dspt2Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt6-Myc_dspt2Cl1_RS.79f53fa4df2b3f795b978b41ff4eb31f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt6-Myc_dspt2Cl1_RS.79f53fa4df2b3f795b978b41ff4eb31f.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS/Spt6-Myc_dspt2Cl1_RS.sorted.bam \
  > alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1_RS/Spt6-Myc_dspt2Cl1_RS.sorted.filtered.bam
samtools_view_filter.Spt6-Myc_dspt2Cl1_RS.79f53fa4df2b3f795b978b41ff4eb31f.mugqic.done
)
samtools_view_filter_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_dspt2Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_7_JOB_ID: samtools_view_filter.Chd1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_spt6_39C_Cl2_RS.b058b6f8350dba2d2b8454911bca0e06.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_spt6_39C_Cl2_RS.b058b6f8350dba2d2b8454911bca0e06.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS/Chd1-Myc_spt6_39C_Cl2_RS.sorted.bam \
  > alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2_RS/Chd1-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_spt6_39C_Cl2_RS.b058b6f8350dba2d2b8454911bca0e06.mugqic.done
)
samtools_view_filter_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_8_JOB_ID: samtools_view_filter.Chd1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_spt6_39C_Cl1_RS.2267956270ea85c77886b713ecd88b48.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_spt6_39C_Cl1_RS.2267956270ea85c77886b713ecd88b48.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS/Chd1-Myc_spt6_39C_Cl1_RS.sorted.bam \
  > alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1_RS/Chd1-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_spt6_39C_Cl1_RS.2267956270ea85c77886b713ecd88b48.mugqic.done
)
samtools_view_filter_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_9_JOB_ID: samtools_view_filter.Iws1-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_spt6_39C_Cl2_RS.43ec38d3b6ee0ac6ffab0856bcd56008.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_spt6_39C_Cl2_RS.43ec38d3b6ee0ac6ffab0856bcd56008.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS/Iws1-Myc_spt6_39C_Cl2_RS.sorted.bam \
  > alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2_RS/Iws1-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_spt6_39C_Cl2_RS.43ec38d3b6ee0ac6ffab0856bcd56008.mugqic.done
)
samtools_view_filter_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_10_JOB_ID: samtools_view_filter.Iws1-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_spt6_39C_Cl1_RS.5c28831f68fd749ea5334b166501332e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_spt6_39C_Cl1_RS.5c28831f68fd749ea5334b166501332e.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS/Iws1-Myc_spt6_39C_Cl1_RS.sorted.bam \
  > alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1_RS/Iws1-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_spt6_39C_Cl1_RS.5c28831f68fd749ea5334b166501332e.mugqic.done
)
samtools_view_filter_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_11_JOB_ID: samtools_view_filter.Spt2-Myc_spt6_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt2-Myc_spt6_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_11_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt2-Myc_spt6_39C_Cl2_RS.74ace3fbb19960954d4cda446274980a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt2-Myc_spt6_39C_Cl2_RS.74ace3fbb19960954d4cda446274980a.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS/Spt2-Myc_spt6_39C_Cl2_RS.sorted.bam \
  > alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2_RS/Spt2-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Spt2-Myc_spt6_39C_Cl2_RS.74ace3fbb19960954d4cda446274980a.mugqic.done
)
samtools_view_filter_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_12_JOB_ID: samtools_view_filter.Spt2-Myc_spt6_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt2-Myc_spt6_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_12_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt2-Myc_spt6_39C_Cl1_RS.df8c9b529c6e9a7a259fe116d27ea165.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt2-Myc_spt6_39C_Cl1_RS.df8c9b529c6e9a7a259fe116d27ea165.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS/Spt2-Myc_spt6_39C_Cl1_RS.sorted.bam \
  > alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1_RS/Spt2-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Spt2-Myc_spt6_39C_Cl1_RS.df8c9b529c6e9a7a259fe116d27ea165.mugqic.done
)
samtools_view_filter_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_spt6_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_13_JOB_ID: samtools_view_filter.Chd1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_13_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_Wt_39C_Cl2_RS.cd655e40e32c551ff7692f7d675b770c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_Wt_39C_Cl2_RS.cd655e40e32c551ff7692f7d675b770c.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS/Chd1-Myc_Wt_39C_Cl2_RS.sorted.bam \
  > alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2_RS/Chd1-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_Wt_39C_Cl2_RS.cd655e40e32c551ff7692f7d675b770c.mugqic.done
)
samtools_view_filter_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_14_JOB_ID: samtools_view_filter.Chd1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_14_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_Wt_39C_Cl1_RS.acb4924315f4bfb67ad495c5ac51e6dd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_Wt_39C_Cl1_RS.acb4924315f4bfb67ad495c5ac51e6dd.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS/Chd1-Myc_Wt_39C_Cl1_RS.sorted.bam \
  > alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1_RS/Chd1-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_Wt_39C_Cl1_RS.acb4924315f4bfb67ad495c5ac51e6dd.mugqic.done
)
samtools_view_filter_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_15_JOB_ID: samtools_view_filter.Iws1-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_15_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_Wt_39C_Cl2_RS.f634c65901aed19af4d2ceeddf64cc53.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_Wt_39C_Cl2_RS.f634c65901aed19af4d2ceeddf64cc53.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS/Iws1-Myc_Wt_39C_Cl2_RS.sorted.bam \
  > alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2_RS/Iws1-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_Wt_39C_Cl2_RS.f634c65901aed19af4d2ceeddf64cc53.mugqic.done
)
samtools_view_filter_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_16_JOB_ID: samtools_view_filter.Iws1-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_16_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_Wt_39C_Cl1_RS.f5a8f2d07ca73a756931b1c467d0cf1b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_Wt_39C_Cl1_RS.f5a8f2d07ca73a756931b1c467d0cf1b.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS/Iws1-Myc_Wt_39C_Cl1_RS.sorted.bam \
  > alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1_RS/Iws1-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_Wt_39C_Cl1_RS.f5a8f2d07ca73a756931b1c467d0cf1b.mugqic.done
)
samtools_view_filter_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_17_JOB_ID: samtools_view_filter.Spt2-Myc_Wt_39C_Cl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt2-Myc_Wt_39C_Cl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_17_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt2-Myc_Wt_39C_Cl2_RS.1fce38309b8a374a0857aa31f1e5b7b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt2-Myc_Wt_39C_Cl2_RS.1fce38309b8a374a0857aa31f1e5b7b3.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS/Spt2-Myc_Wt_39C_Cl2_RS.sorted.bam \
  > alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2_RS/Spt2-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam
samtools_view_filter.Spt2-Myc_Wt_39C_Cl2_RS.1fce38309b8a374a0857aa31f1e5b7b3.mugqic.done
)
samtools_view_filter_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_18_JOB_ID: samtools_view_filter.Spt2-Myc_Wt_39C_Cl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt2-Myc_Wt_39C_Cl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_18_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt2-Myc_Wt_39C_Cl1_RS.0a246e691fe690ce9601422018eb38e0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt2-Myc_Wt_39C_Cl1_RS.0a246e691fe690ce9601422018eb38e0.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS/Spt2-Myc_Wt_39C_Cl1_RS.sorted.bam \
  > alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1_RS/Spt2-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam
samtools_view_filter.Spt2-Myc_Wt_39C_Cl1_RS.0a246e691fe690ce9601422018eb38e0.mugqic.done
)
samtools_view_filter_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt2-Myc_Wt_39C_Cl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_19_JOB_ID: samtools_view_filter.Chd1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_WtCl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_19_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_WtCl2_RS.177b3086ff68caf493e3559eb944b173.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_WtCl2_RS.177b3086ff68caf493e3559eb944b173.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS/Chd1-Myc_WtCl2_RS.sorted.bam \
  > alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2_RS/Chd1-Myc_WtCl2_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_WtCl2_RS.177b3086ff68caf493e3559eb944b173.mugqic.done
)
samtools_view_filter_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_20_JOB_ID: samtools_view_filter.Chd1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Chd1-Myc_WtCl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_20_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Chd1-Myc_WtCl1_RS.388f9e11b27c0822ec6f87b2058351f3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Chd1-Myc_WtCl1_RS.388f9e11b27c0822ec6f87b2058351f3.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS/Chd1-Myc_WtCl1_RS.sorted.bam \
  > alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1_RS/Chd1-Myc_WtCl1_RS.sorted.filtered.bam
samtools_view_filter.Chd1-Myc_WtCl1_RS.388f9e11b27c0822ec6f87b2058351f3.mugqic.done
)
samtools_view_filter_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Chd1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_21_JOB_ID: samtools_view_filter.Iws1-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_WtCl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_21_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_WtCl2_RS.de41c8f2e05a7c34be137ac44e440ca7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_WtCl2_RS.de41c8f2e05a7c34be137ac44e440ca7.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS/Iws1-Myc_WtCl2_RS.sorted.bam \
  > alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2_RS/Iws1-Myc_WtCl2_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_WtCl2_RS.de41c8f2e05a7c34be137ac44e440ca7.mugqic.done
)
samtools_view_filter_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_22_JOB_ID: samtools_view_filter.Iws1-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Iws1-Myc_WtCl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_22_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Iws1-Myc_WtCl1_RS.7df5ebd1ab307f1c9b51189c0225ac0b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Iws1-Myc_WtCl1_RS.7df5ebd1ab307f1c9b51189c0225ac0b.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS/Iws1-Myc_WtCl1_RS.sorted.bam \
  > alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1_RS/Iws1-Myc_WtCl1_RS.sorted.filtered.bam
samtools_view_filter.Iws1-Myc_WtCl1_RS.7df5ebd1ab307f1c9b51189c0225ac0b.mugqic.done
)
samtools_view_filter_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Iws1-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_23_JOB_ID: samtools_view_filter.Spt6-Myc_WtCl2_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt6-Myc_WtCl2_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_23_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt6-Myc_WtCl2_RS.08d380bcf9ad84604a21d8e91dbdffe3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt6-Myc_WtCl2_RS.08d380bcf9ad84604a21d8e91dbdffe3.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS/Spt6-Myc_WtCl2_RS.sorted.bam \
  > alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2_RS/Spt6-Myc_WtCl2_RS.sorted.filtered.bam
samtools_view_filter.Spt6-Myc_WtCl2_RS.08d380bcf9ad84604a21d8e91dbdffe3.mugqic.done
)
samtools_view_filter_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_24_JOB_ID: samtools_view_filter.Spt6-Myc_WtCl1_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.Spt6-Myc_WtCl1_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_24_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.Spt6-Myc_WtCl1_RS.a2644f772f232503a00bc83c053cd867.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.Spt6-Myc_WtCl1_RS.a2644f772f232503a00bc83c053cd867.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS/Spt6-Myc_WtCl1_RS.sorted.bam \
  > alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1_RS/Spt6-Myc_WtCl1_RS.sorted.filtered.bam
samtools_view_filter.Spt6-Myc_WtCl1_RS.a2644f772f232503a00bc83c053cd867.mugqic.done
)
samtools_view_filter_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/Spt6-Myc_WtCl1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_25_JOB_ID: samtools_view_filter.No-TAG_RS
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.No-TAG_RS
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_25_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.No-TAG_RS.697045ea359966784ac0d43a86a900e2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.No-TAG_RS.697045ea359966784ac0d43a86a900e2.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/No-TAG/No-TAG_RS/No-TAG_RS.sorted.bam \
  > alignment/No-TAG/No-TAG_RS/No-TAG_RS.sorted.filtered.bam
samtools_view_filter.No-TAG_RS.697045ea359966784ac0d43a86a900e2.mugqic.done
)
samtools_view_filter_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \"running\"
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/home/efournie/genpipes/resources/genomes/config/Saccharomyces_cerevisiae.R64-1-1.ini,input/chipseq.numpy.bug.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6007406/efournie/Chaperone/output/pipeline/json/No-TAG.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.8 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_26_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID:$samtools_view_filter_4_JOB_ID:$samtools_view_filter_5_JOB_ID:$samtools_view_filter_6_JOB_ID:$samtools_view_filter_7_JOB_ID:$samtools_view_filter_8_JOB_ID:$samtools_view_filter_9_JOB_ID:$samtools_view_filter_10_JOB_ID:$samtools_view_filter_11_JOB_ID:$samtools_view_filter_12_JOB_ID:$samtools_view_filter_13_JOB_ID:$samtools_view_filter_14_JOB_ID:$samtools_view_filter_15_JOB_ID:$samtools_view_filter_16_JOB_ID:$samtools_view_filter_17_JOB_ID:$samtools_view_filter_18_JOB_ID:$samtools_view_filter_19_JOB_ID:$samtools_view_filter_20_JOB_ID:$samtools_view_filter_21_JOB_ID:$samtools_view_filter_22_JOB_ID:$samtools_view_filter_23_JOB_ID:$samtools_view_filter_24_JOB_ID:$samtools_view_filter_25_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /home/efournie/genpipes/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done
)
samtools_view_filter_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_dspt2Cl2
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_dspt2Cl2.08695a95482a565cf662aa1051209819.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_dspt2Cl2.08695a95482a565cf662aa1051209819.mugqic.done'
mkdir -p alignment/Chd1-Myc_dspt2Cl2 && \
ln -s -f Chd1-Myc_dspt2Cl2_RS/Chd1-Myc_dspt2Cl2_RS.sorted.filtered.bam alignment/Chd1-Myc_dspt2Cl2/Chd1-Myc_dspt2Cl2.merged.bam
symlink_readset_sample_bam.Chd1-Myc_dspt2Cl2.08695a95482a565cf662aa1051209819.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_dspt2Cl1
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_dspt2Cl1.9e54c6f428008b780f092c083c45fdaf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_dspt2Cl1.9e54c6f428008b780f092c083c45fdaf.mugqic.done'
mkdir -p alignment/Chd1-Myc_dspt2Cl1 && \
ln -s -f Chd1-Myc_dspt2Cl1_RS/Chd1-Myc_dspt2Cl1_RS.sorted.filtered.bam alignment/Chd1-Myc_dspt2Cl1/Chd1-Myc_dspt2Cl1.merged.bam
symlink_readset_sample_bam.Chd1-Myc_dspt2Cl1.9e54c6f428008b780f092c083c45fdaf.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_dspt2Cl2
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_dspt2Cl2.b05db6778d851b8ee7a21a9e03dd9ccc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_dspt2Cl2.b05db6778d851b8ee7a21a9e03dd9ccc.mugqic.done'
mkdir -p alignment/Iws1-Myc_dspt2Cl2 && \
ln -s -f Iws1-Myc_dspt2Cl2_RS/Iws1-Myc_dspt2Cl2_RS.sorted.filtered.bam alignment/Iws1-Myc_dspt2Cl2/Iws1-Myc_dspt2Cl2.merged.bam
symlink_readset_sample_bam.Iws1-Myc_dspt2Cl2.b05db6778d851b8ee7a21a9e03dd9ccc.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_4_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_dspt2Cl1
JOB_DEPENDENCIES=$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_dspt2Cl1.08d896e74c28874269c56f1265c372f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_dspt2Cl1.08d896e74c28874269c56f1265c372f9.mugqic.done'
mkdir -p alignment/Iws1-Myc_dspt2Cl1 && \
ln -s -f Iws1-Myc_dspt2Cl1_RS/Iws1-Myc_dspt2Cl1_RS.sorted.filtered.bam alignment/Iws1-Myc_dspt2Cl1/Iws1-Myc_dspt2Cl1.merged.bam
symlink_readset_sample_bam.Iws1-Myc_dspt2Cl1.08d896e74c28874269c56f1265c372f9.mugqic.done
)
picard_merge_sam_files_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_5_JOB_ID: symlink_readset_sample_bam.Spt6-Myc_dspt2Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt6-Myc_dspt2Cl2
JOB_DEPENDENCIES=$samtools_view_filter_5_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt6-Myc_dspt2Cl2.092145fad36710113d76940360665842.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt6-Myc_dspt2Cl2.092145fad36710113d76940360665842.mugqic.done'
mkdir -p alignment/Spt6-Myc_dspt2Cl2 && \
ln -s -f Spt6-Myc_dspt2Cl2_RS/Spt6-Myc_dspt2Cl2_RS.sorted.filtered.bam alignment/Spt6-Myc_dspt2Cl2/Spt6-Myc_dspt2Cl2.merged.bam
symlink_readset_sample_bam.Spt6-Myc_dspt2Cl2.092145fad36710113d76940360665842.mugqic.done
)
picard_merge_sam_files_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_6_JOB_ID: symlink_readset_sample_bam.Spt6-Myc_dspt2Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt6-Myc_dspt2Cl1
JOB_DEPENDENCIES=$samtools_view_filter_6_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt6-Myc_dspt2Cl1.66532f6bf238d720701602a9a4e8e4b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt6-Myc_dspt2Cl1.66532f6bf238d720701602a9a4e8e4b5.mugqic.done'
mkdir -p alignment/Spt6-Myc_dspt2Cl1 && \
ln -s -f Spt6-Myc_dspt2Cl1_RS/Spt6-Myc_dspt2Cl1_RS.sorted.filtered.bam alignment/Spt6-Myc_dspt2Cl1/Spt6-Myc_dspt2Cl1.merged.bam
symlink_readset_sample_bam.Spt6-Myc_dspt2Cl1.66532f6bf238d720701602a9a4e8e4b5.mugqic.done
)
picard_merge_sam_files_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_7_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_7_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl2.2d56b9777f6c5a88b9fdc5c07d956ee1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl2.2d56b9777f6c5a88b9fdc5c07d956ee1.mugqic.done'
mkdir -p alignment/Chd1-Myc_spt6_39C_Cl2 && \
ln -s -f Chd1-Myc_spt6_39C_Cl2_RS/Chd1-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam alignment/Chd1-Myc_spt6_39C_Cl2/Chd1-Myc_spt6_39C_Cl2.merged.bam
symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl2.2d56b9777f6c5a88b9fdc5c07d956ee1.mugqic.done
)
picard_merge_sam_files_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_8_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_8_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl1.82e9a621f7e8a092c101fdbf541766a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl1.82e9a621f7e8a092c101fdbf541766a2.mugqic.done'
mkdir -p alignment/Chd1-Myc_spt6_39C_Cl1 && \
ln -s -f Chd1-Myc_spt6_39C_Cl1_RS/Chd1-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam alignment/Chd1-Myc_spt6_39C_Cl1/Chd1-Myc_spt6_39C_Cl1.merged.bam
symlink_readset_sample_bam.Chd1-Myc_spt6_39C_Cl1.82e9a621f7e8a092c101fdbf541766a2.mugqic.done
)
picard_merge_sam_files_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_9_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_9_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl2.a22249122153c2f387a13076ce2e830f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl2.a22249122153c2f387a13076ce2e830f.mugqic.done'
mkdir -p alignment/Iws1-Myc_spt6_39C_Cl2 && \
ln -s -f Iws1-Myc_spt6_39C_Cl2_RS/Iws1-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam alignment/Iws1-Myc_spt6_39C_Cl2/Iws1-Myc_spt6_39C_Cl2.merged.bam
symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl2.a22249122153c2f387a13076ce2e830f.mugqic.done
)
picard_merge_sam_files_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_10_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_10_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl1.c0b9b97cce97d82ee2e17ab541e91774.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl1.c0b9b97cce97d82ee2e17ab541e91774.mugqic.done'
mkdir -p alignment/Iws1-Myc_spt6_39C_Cl1 && \
ln -s -f Iws1-Myc_spt6_39C_Cl1_RS/Iws1-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam alignment/Iws1-Myc_spt6_39C_Cl1/Iws1-Myc_spt6_39C_Cl1.merged.bam
symlink_readset_sample_bam.Iws1-Myc_spt6_39C_Cl1.c0b9b97cce97d82ee2e17ab541e91774.mugqic.done
)
picard_merge_sam_files_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_11_JOB_ID: symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_11_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl2.43d5d22712411a73a141c5c48be205cc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl2.43d5d22712411a73a141c5c48be205cc.mugqic.done'
mkdir -p alignment/Spt2-Myc_spt6_39C_Cl2 && \
ln -s -f Spt2-Myc_spt6_39C_Cl2_RS/Spt2-Myc_spt6_39C_Cl2_RS.sorted.filtered.bam alignment/Spt2-Myc_spt6_39C_Cl2/Spt2-Myc_spt6_39C_Cl2.merged.bam
symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl2.43d5d22712411a73a141c5c48be205cc.mugqic.done
)
picard_merge_sam_files_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_12_JOB_ID: symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_12_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl1.398f1ce112e52dacf4c8b082c9684adc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl1.398f1ce112e52dacf4c8b082c9684adc.mugqic.done'
mkdir -p alignment/Spt2-Myc_spt6_39C_Cl1 && \
ln -s -f Spt2-Myc_spt6_39C_Cl1_RS/Spt2-Myc_spt6_39C_Cl1_RS.sorted.filtered.bam alignment/Spt2-Myc_spt6_39C_Cl1/Spt2-Myc_spt6_39C_Cl1.merged.bam
symlink_readset_sample_bam.Spt2-Myc_spt6_39C_Cl1.398f1ce112e52dacf4c8b082c9684adc.mugqic.done
)
picard_merge_sam_files_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_13_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_13_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl2.4ee753ecb220148a512d122cdb277585.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl2.4ee753ecb220148a512d122cdb277585.mugqic.done'
mkdir -p alignment/Chd1-Myc_Wt_39C_Cl2 && \
ln -s -f Chd1-Myc_Wt_39C_Cl2_RS/Chd1-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam alignment/Chd1-Myc_Wt_39C_Cl2/Chd1-Myc_Wt_39C_Cl2.merged.bam
symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl2.4ee753ecb220148a512d122cdb277585.mugqic.done
)
picard_merge_sam_files_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_14_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_14_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl1.b8300dd0add3aa6123d5e295b88caf38.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl1.b8300dd0add3aa6123d5e295b88caf38.mugqic.done'
mkdir -p alignment/Chd1-Myc_Wt_39C_Cl1 && \
ln -s -f Chd1-Myc_Wt_39C_Cl1_RS/Chd1-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam alignment/Chd1-Myc_Wt_39C_Cl1/Chd1-Myc_Wt_39C_Cl1.merged.bam
symlink_readset_sample_bam.Chd1-Myc_Wt_39C_Cl1.b8300dd0add3aa6123d5e295b88caf38.mugqic.done
)
picard_merge_sam_files_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_15_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_15_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl2.e77e543a8c567f82c134feff79b8f0bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl2.e77e543a8c567f82c134feff79b8f0bd.mugqic.done'
mkdir -p alignment/Iws1-Myc_Wt_39C_Cl2 && \
ln -s -f Iws1-Myc_Wt_39C_Cl2_RS/Iws1-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam alignment/Iws1-Myc_Wt_39C_Cl2/Iws1-Myc_Wt_39C_Cl2.merged.bam
symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl2.e77e543a8c567f82c134feff79b8f0bd.mugqic.done
)
picard_merge_sam_files_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_16_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_16_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl1.f3c6fbbe6de6b974135d4dadaf4c7ec3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl1.f3c6fbbe6de6b974135d4dadaf4c7ec3.mugqic.done'
mkdir -p alignment/Iws1-Myc_Wt_39C_Cl1 && \
ln -s -f Iws1-Myc_Wt_39C_Cl1_RS/Iws1-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam alignment/Iws1-Myc_Wt_39C_Cl1/Iws1-Myc_Wt_39C_Cl1.merged.bam
symlink_readset_sample_bam.Iws1-Myc_Wt_39C_Cl1.f3c6fbbe6de6b974135d4dadaf4c7ec3.mugqic.done
)
picard_merge_sam_files_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_17_JOB_ID: symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_17_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl2.946bf69493e641be083b3b44b0e70422.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl2.946bf69493e641be083b3b44b0e70422.mugqic.done'
mkdir -p alignment/Spt2-Myc_Wt_39C_Cl2 && \
ln -s -f Spt2-Myc_Wt_39C_Cl2_RS/Spt2-Myc_Wt_39C_Cl2_RS.sorted.filtered.bam alignment/Spt2-Myc_Wt_39C_Cl2/Spt2-Myc_Wt_39C_Cl2.merged.bam
symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl2.946bf69493e641be083b3b44b0e70422.mugqic.done
)
picard_merge_sam_files_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_18_JOB_ID: symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_18_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl1.3e42f92f122c9b2d192f7648db9aca16.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl1.3e42f92f122c9b2d192f7648db9aca16.mugqic.done'
mkdir -p alignment/Spt2-Myc_Wt_39C_Cl1 && \
ln -s -f Spt2-Myc_Wt_39C_Cl1_RS/Spt2-Myc_Wt_39C_Cl1_RS.sorted.filtered.bam alignment/Spt2-Myc_Wt_39C_Cl1/Spt2-Myc_Wt_39C_Cl1.merged.bam
symlink_readset_sample_bam.Spt2-Myc_Wt_39C_Cl1.3e42f92f122c9b2d192f7648db9aca16.mugqic.done
)
picard_merge_sam_files_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_19_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_WtCl2
JOB_DEPENDENCIES=$samtools_view_filter_19_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_WtCl2.1ebf6bc21f19a010b021191f5f081a91.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_WtCl2.1ebf6bc21f19a010b021191f5f081a91.mugqic.done'
mkdir -p alignment/Chd1-Myc_WtCl2 && \
ln -s -f Chd1-Myc_WtCl2_RS/Chd1-Myc_WtCl2_RS.sorted.filtered.bam alignment/Chd1-Myc_WtCl2/Chd1-Myc_WtCl2.merged.bam
symlink_readset_sample_bam.Chd1-Myc_WtCl2.1ebf6bc21f19a010b021191f5f081a91.mugqic.done
)
picard_merge_sam_files_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_20_JOB_ID: symlink_readset_sample_bam.Chd1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Chd1-Myc_WtCl1
JOB_DEPENDENCIES=$samtools_view_filter_20_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Chd1-Myc_WtCl1.8fee36755ee2cfa5b712d10b8ba5e23d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Chd1-Myc_WtCl1.8fee36755ee2cfa5b712d10b8ba5e23d.mugqic.done'
mkdir -p alignment/Chd1-Myc_WtCl1 && \
ln -s -f Chd1-Myc_WtCl1_RS/Chd1-Myc_WtCl1_RS.sorted.filtered.bam alignment/Chd1-Myc_WtCl1/Chd1-Myc_WtCl1.merged.bam
symlink_readset_sample_bam.Chd1-Myc_WtCl1.8fee36755ee2cfa5b712d10b8ba5e23d.mugqic.done
)
picard_merge_sam_files_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_21_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_WtCl2
JOB_DEPENDENCIES=$samtools_view_filter_21_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_WtCl2.b67983cf979150bf2c604d62fde72a20.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_WtCl2.b67983cf979150bf2c604d62fde72a20.mugqic.done'
mkdir -p alignment/Iws1-Myc_WtCl2 && \
ln -s -f Iws1-Myc_WtCl2_RS/Iws1-Myc_WtCl2_RS.sorted.filtered.bam alignment/Iws1-Myc_WtCl2/Iws1-Myc_WtCl2.merged.bam
symlink_readset_sample_bam.Iws1-Myc_WtCl2.b67983cf979150bf2c604d62fde72a20.mugqic.done
)
picard_merge_sam_files_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_22_JOB_ID: symlink_readset_sample_bam.Iws1-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Iws1-Myc_WtCl1
JOB_DEPENDENCIES=$samtools_view_filter_22_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Iws1-Myc_WtCl1.692d9e3f6086b46c1430ae0ccb8a38d6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Iws1-Myc_WtCl1.692d9e3f6086b46c1430ae0ccb8a38d6.mugqic.done'
mkdir -p alignment/Iws1-Myc_WtCl1 && \
ln -s -f Iws1-Myc_WtCl1_RS/Iws1-Myc_WtCl1_RS.sorted.filtered.bam alignment/Iws1-Myc_WtCl1/Iws1-Myc_WtCl1.merged.bam
symlink_readset_sample_bam.Iws1-Myc_WtCl1.692d9e3f6086b46c1430ae0ccb8a38d6.mugqic.done
)
picard_merge_sam_files_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_23_JOB_ID: symlink_readset_sample_bam.Spt6-Myc_WtCl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt6-Myc_WtCl2
JOB_DEPENDENCIES=$samtools_view_filter_23_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt6-Myc_WtCl2.296a03a111350a7499b776c1df0833cc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt6-Myc_WtCl2.296a03a111350a7499b776c1df0833cc.mugqic.done'
mkdir -p alignment/Spt6-Myc_WtCl2 && \
ln -s -f Spt6-Myc_WtCl2_RS/Spt6-Myc_WtCl2_RS.sorted.filtered.bam alignment/Spt6-Myc_WtCl2/Spt6-Myc_WtCl2.merged.bam
symlink_readset_sample_bam.Spt6-Myc_WtCl2.296a03a111350a7499b776c1df0833cc.mugqic.done
)
picard_merge_sam_files_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_24_JOB_ID: symlink_readset_sample_bam.Spt6-Myc_WtCl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Spt6-Myc_WtCl1
JOB_DEPENDENCIES=$samtools_view_filter_24_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Spt6-Myc_WtCl1.86c52104f445c463684d8f2662355982.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Spt6-Myc_WtCl1.86c52104f445c463684d8f2662355982.mugqic.done'
mkdir -p alignment/Spt6-Myc_WtCl1 && \
ln -s -f Spt6-Myc_WtCl1_RS/Spt6-Myc_WtCl1_RS.sorted.filtered.bam alignment/Spt6-Myc_WtCl1/Spt6-Myc_WtCl1.merged.bam
symlink_readset_sample_bam.Spt6-Myc_WtCl1.86c52104f445c463684d8f2662355982.mugqic.done
)
picard_merge_sam_files_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_25_JOB_ID: symlink_readset_sample_bam.No-TAG
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.No-TAG
JOB_DEPENDENCIES=$samtools_view_filter_25_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.No-TAG.1d11f6cab239a528a1556b9b3117d813.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.No-TAG.1d11f6cab239a528a1556b9b3117d813.mugqic.done'
mkdir -p alignment/No-TAG && \
ln -s -f No-TAG_RS/No-TAG_RS.sorted.filtered.bam alignment/No-TAG/No-TAG.merged.bam
symlink_readset_sample_bam.No-TAG.1d11f6cab239a528a1556b9b3117d813.mugqic.done
)
picard_merge_sam_files_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files&samples=25&AnonymizedList=629a1d1684d7a98c333e21201a34ba1b,8cf7fd541adb693a728835efdcb8c8f1,a7b5ee157403acbfedb6380c58652cb5,876397d96b246ed5717c3b37bf8730dc,3168c8aac8b7814d00e0b680f9354f97,ea4caba859b4b4dbf52820c3b48c6f97,880adfc18058c33b53708314a85e992e,946886ff3d68654cb57e5bcf89f8e2ac,2ec7373bbaa79f31f83a08e615489067,5eadc537e5e4fafd15080f3b5ecc3d49,b1539d4e28c9818cb417e5ff312ae426,e868a7050203d7a134ccf5039656e438,6b52c4728975b40e433e458aab6bda84,f9f61cc2a3cb3e763aeb7343adb11cce,d425155504a53ff5e0cd0b1039452203,d72ca478694525164f3eec5bde523e02,ce11ed096df848991c2288a8b62a8d97,3648f9bcbab381e306e75aebf126343f,be429282b5bef7301d60bf822b5151b7,7b526dfd579fe9922ae06d661889fcce,e803a942d00199e83bb64dda5df9cb69,36fffc09dc635b4f768ab3b3081a8118,e1ae5e7a4af208160065396d8f8e9e0d,5a7465b6e044c6d807679784c2f2c161,8118bb5d1acc8f1a7160ecc2ba4bfdb3" --quiet --output-document=/dev/null

