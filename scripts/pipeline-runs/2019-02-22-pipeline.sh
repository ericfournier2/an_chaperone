#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.1.2-beta
# Created on: 2019-02-22T13:52:26
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 3 jobs
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 4 jobs
#   samtools_view_filter: 4 jobs
#   picard_merge_sam_files: 3 jobs
#   picard_mark_duplicates: 4 jobs
#   TOTAL: 19 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6007406/efournie/HistExchange/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.base.ini,/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/Saccharomyces_cerevisiae.R64-1-1.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.9ea7edba6df52d20aa65fef6f4b65212.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.9ea7edba6df52d20aa65fef6f4b65212.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Flag_CK2_Cl2 && \
`cat > trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/HistExchange/raw/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz \
  trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.log
trimmomatic.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.9ea7edba6df52d20aa65fef6f4b65212.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.fdffe5ae3c5aa5550e3c68a0d580c6eb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.fdffe5ae3c5aa5550e3c68a0d580c6eb.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Flag_CK2_Cl1 && \
`cat > trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/HistExchange/raw/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz \
  trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.log
trimmomatic.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.fdffe5ae3c5aa5550e3c68a0d580c6eb.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.858d54b837a4da585c7f2f24e80b9ead.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.858d54b837a4da585c7f2f24e80b9ead.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/Flag_WT_Cl1 && \
`cat > trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/HistExchange/raw/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz \
  trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.log
trimmomatic.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.858d54b837a4da585c7f2f24e80b9ead.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.5ce1f8a490e493ee61028f54f171f040.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.5ce1f8a490e493ee61028f54f171f040.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Flag_CK2_Cl2	HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Flag_CK2_Cl1	HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Flag_WT_Cl2/HI.1716.007.Index_13.Flag_WT_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Flag_WT_Cl2	HI.1716.007.Index_13.Flag_WT_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Flag_WT_Cl1	HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K56ac_CK2_Cl2/HI.1716.007.Index_16.H3K56ac_CK2_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K56ac_CK2_Cl2	HI.1716.007.Index_16.H3K56ac_CK2_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K56ac_CK2_Cl1/HI.1716.007.Index_14.H3K56ac_CK2_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K56ac_CK2_Cl1	HI.1716.007.Index_14.H3K56ac_CK2_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K56ac_WT_Cl2/HI.1716.007.Index_18.H3K56ac_WT_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K56ac_WT_Cl2	HI.1716.007.Index_18.H3K56ac_WT_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K56ac_WT_Cl1/HI.1716.007.Index_7.H3K56ac_WT_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K56ac_WT_Cl1	HI.1716.007.Index_7.H3K56ac_WT_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Rnap2_Ck2_Cl1/HI.1716.007.Index_12.Rnap2_Ck2_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Rnap2_Ck2_Cl1	HI.1716.007.Index_12.Rnap2_Ck2_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Rnap2_Ck2_Cl2/HI.1716.007.Index_19.Rnap2_Ck2_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Rnap2_Ck2_Cl2	HI.1716.007.Index_19.Rnap2_Ck2_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Rnap2_Wt_Cl1/HI.1716.007.Index_4.Rnap2_Wt_Cl1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Rnap2_Wt_Cl1	HI.1716.007.Index_4.Rnap2_Wt_Cl1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Rnap2_Wt_Cl2/HI.1716.007.Index_5.Rnap2_Wt_Cl2_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Rnap2_Wt_Cl2	HI.1716.007.Index_5.Rnap2_Wt_Cl2_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.5ce1f8a490e493ee61028f54f171f040.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.0280046b71de8d87f77f8c91bf7aba75.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.0280046b71de8d87f77f8c91bf7aba75.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz	SM:Flag_CK2_Cl2	LB:Flag_CK2_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.0280046b71de8d87f77f8c91bf7aba75.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.2db2554a14d5095972608d1a0cc0d939.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.2db2554a14d5095972608d1a0cc0d939.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz	SM:Flag_CK2_Cl1	LB:Flag_CK2_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.2db2554a14d5095972608d1a0cc0d939.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_3_JOB_ID: bwa_mem_picard_sort_sam.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.1043969b9a998e8d801663ee1cb156f7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.1043969b9a998e8d801663ee1cb156f7.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz	SM:Flag_WT_Cl1	LB:Flag_WT_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.1043969b9a998e8d801663ee1cb156f7.mugqic.done
)
bwa_mem_picard_sort_sam_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_4_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID:$bwa_mem_picard_sort_sam_2_JOB_ID:$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Saccharomyces_cerevisiae" \
  --variable assembly="R64-1-1" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
)
bwa_mem_picard_sort_sam_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.ab205c5c3578d6d0251dc6d8c3814918.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.ab205c5c3578d6d0251dc6d8c3814918.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.sorted.bam \
  > alignment/Flag_CK2_Cl2/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.ab205c5c3578d6d0251dc6d8c3814918.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.bf388c5f88bcd7b540081133e6ff4d69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.bf388c5f88bcd7b540081133e6ff4d69.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.sorted.bam \
  > alignment/Flag_CK2_Cl1/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.bf388c5f88bcd7b540081133e6ff4d69.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.522b1a8301320a6bb4ace110d53c850c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.522b1a8301320a6bb4ace110d53c850c.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.sorted.bam \
  > alignment/Flag_WT_Cl1/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.522b1a8301320a6bb4ace110d53c850c.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.Flag_CK2_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Flag_CK2_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Flag_CK2_Cl2.66c9eafd4883b8be8f8a87a5d711bbcf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Flag_CK2_Cl2.66c9eafd4883b8be8f8a87a5d711bbcf.mugqic.done'
mkdir -p alignment/Flag_CK2_Cl2 && \
ln -s -f HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz/HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz.sorted.filtered.bam alignment/Flag_CK2_Cl2/Flag_CK2_Cl2.merged.bam
symlink_readset_sample_bam.Flag_CK2_Cl2.66c9eafd4883b8be8f8a87a5d711bbcf.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.Flag_CK2_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Flag_CK2_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Flag_CK2_Cl1.edbad35acc23dfce9447ee040ac6a764.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Flag_CK2_Cl1.edbad35acc23dfce9447ee040ac6a764.mugqic.done'
mkdir -p alignment/Flag_CK2_Cl1 && \
ln -s -f HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz/HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz.sorted.filtered.bam alignment/Flag_CK2_Cl1/Flag_CK2_Cl1.merged.bam
symlink_readset_sample_bam.Flag_CK2_Cl1.edbad35acc23dfce9447ee040ac6a764.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.Flag_WT_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Flag_WT_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Flag_WT_Cl1.95fe7a0367b8e85898e87a6b17deeeee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Flag_WT_Cl1.95fe7a0367b8e85898e87a6b17deeeee.mugqic.done'
mkdir -p alignment/Flag_WT_Cl1 && \
ln -s -f HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz/HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz.sorted.filtered.bam alignment/Flag_WT_Cl1/Flag_WT_Cl1.merged.bam
symlink_readset_sample_bam.Flag_WT_Cl1.95fe7a0367b8e85898e87a6b17deeeee.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.Flag_CK2_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Flag_CK2_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Flag_CK2_Cl2.8ee90e071946d3e0279cd5a2d0ff4d29.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Flag_CK2_Cl2.8ee90e071946d3e0279cd5a2d0ff4d29.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Flag_CK2_Cl2/Flag_CK2_Cl2.merged.bam \
 OUTPUT=alignment/Flag_CK2_Cl2/Flag_CK2_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/Flag_CK2_Cl2/Flag_CK2_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Flag_CK2_Cl2.8ee90e071946d3e0279cd5a2d0ff4d29.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.Flag_CK2_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Flag_CK2_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Flag_CK2_Cl1.70d3ce8cf7ae68e46e3f35b5f40f293d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Flag_CK2_Cl1.70d3ce8cf7ae68e46e3f35b5f40f293d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Flag_CK2_Cl1/Flag_CK2_Cl1.merged.bam \
 OUTPUT=alignment/Flag_CK2_Cl1/Flag_CK2_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/Flag_CK2_Cl1/Flag_CK2_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Flag_CK2_Cl1.70d3ce8cf7ae68e46e3f35b5f40f293d.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.Flag_WT_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Flag_WT_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Flag_WT_Cl1.e46ec83ab504132f190d1f54386edbb9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Flag_WT_Cl1.e46ec83ab504132f190d1f54386edbb9.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Flag_WT_Cl1/Flag_WT_Cl1.merged.bam \
 OUTPUT=alignment/Flag_WT_Cl1/Flag_WT_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/Flag_WT_Cl1/Flag_WT_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Flag_WT_Cl1.e46ec83ab504132f190d1f54386edbb9.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
LOG_MD5=$(echo $USER-'206.12.124.2-ChipSeq-Rnap2_Ck2_Cl1.HI.1716.007.Index_12.Rnap2_Ck2_Cl1_R1.fastq.gz,Rnap2_Ck2_Cl2.HI.1716.007.Index_19.Rnap2_Ck2_Cl2_R1.fastq.gz,Flag_WT_Cl1.HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz,Flag_WT_Cl2.HI.1716.007.Index_13.Flag_WT_Cl2_R1.fastq.gz,H3K56ac_CK2_Cl2.HI.1716.007.Index_16.H3K56ac_CK2_Cl2_R1.fastq.gz,H3K56ac_CK2_Cl1.HI.1716.007.Index_14.H3K56ac_CK2_Cl1_R1.fastq.gz,Rnap2_Wt_Cl1.HI.1716.007.Index_4.Rnap2_Wt_Cl1_R1.fastq.gz,Rnap2_Wt_Cl2.HI.1716.007.Index_5.Rnap2_Wt_Cl2_R1.fastq.gz,Flag_CK2_Cl1.HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz,H3K56ac_WT_Cl2.HI.1716.007.Index_18.H3K56ac_WT_Cl2_R1.fastq.gz,H3K56ac_WT_Cl1.HI.1716.007.Index_7.H3K56ac_WT_Cl1_R1.fastq.gz,Flag_CK2_Cl2.HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz' | md5sum | awk '{ print $1 }')
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates&samples=12&md5=$LOG_MD5" --quiet --output-document=/dev/null

