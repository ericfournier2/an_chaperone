export RAP_ID="def-amnou"

mkdir -p output/pipeline-exchange
module load mugqic/genpipes/3.1.2

chipseq.py -j slurm -s '1-7' \
    -l debug \
    -r raw/HistExchange/readset.txt \
    -d raw/HistExchange/design.txt \
    -o output/pipeline-exchange \
    --config $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.cedar.ini \
        $MUGQIC_INSTALL_HOME/genomes/species/Saccharomyces_cerevisiae.R64-1-1/Saccharomyces_cerevisiae.R64-1-1.ini        