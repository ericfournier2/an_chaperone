Sample	Strain	Antibody	Replicate	Readset	RunType	QualityOffset	FASTQ1	Adapter1
Flag_CK2_Cl2	CK2	Flag	2	HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_15.Flag_CK2_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Flag_CK2_Cl1	CK2	Flag	1	HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_6.Flag_CK2_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Flag_WT_Cl2	WT	Flag	2	HI.1716.007.Index_13.Flag_WT_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_13.Flag_WT_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Flag_WT_Cl1	WT	Flag	1	HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_2.Flag_WT_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
H3K56ac_CK2_Cl2	CK2	H3K56ac	2	HI.1716.007.Index_16.H3K56ac_CK2_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_16.H3K56ac_CK2_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
H3K56ac_CK2_Cl1	CK2	H3K56ac	1	HI.1716.007.Index_14.H3K56ac_CK2_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_14.H3K56ac_CK2_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
H3K56ac_WT_Cl2	WT	H3K56ac	2	HI.1716.007.Index_18.H3K56ac_WT_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_18.H3K56ac_WT_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
H3K56ac_WT_Cl1	WT	H3K56ac	1	HI.1716.007.Index_7.H3K56ac_WT_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_7.H3K56ac_WT_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Rnap2_Ck2_Cl1	CK2	Rnap2	1	HI.1716.007.Index_12.Rnap2_Ck2_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_12.Rnap2_Ck2_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Rnap2_Ck2_Cl2	CK2	Rnap2	2	HI.1716.007.Index_19.Rnap2_Ck2_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_19.Rnap2_Ck2_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Rnap2_Wt_Cl1	WT	Rnap2	1	HI.1716.007.Index_4.Rnap2_Wt_Cl1_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_4.Rnap2_Wt_Cl1_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
Rnap2_Wt_Cl2	WT	Rnap2	2	HI.1716.007.Index_5.Rnap2_Wt_Cl2_R1.fastq.gz	SINGLE_END	33	HI.1716.007.Index_5.Rnap2_Wt_Cl2_R1.fastq.gz	AGATCGGAAGAGCACACGTCTGAACTCCAGTCA

